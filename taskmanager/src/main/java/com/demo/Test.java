package com.demo;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;


public class Test {
  public static void main(String[] args) throws IOException, InterruptedException {

      Process p;
      StringBuilder sb = new StringBuilder();

      p = Runtime.getRuntime().exec("quser");
      p.waitFor();

      BufferedReader reader
          = new BufferedReader(new InputStreamReader(p.getInputStream()));

      String line = "";
      while ((line = reader.readLine()) != null) {
        sb.append(line + "\n");
      }
      System.out.println(sb);
    String[] vals = sb.toString().split(" ");
    String loginTime = vals[vals.length - 3] + " " + vals[vals.length - 2] + " " + vals[vals.length - 1];
    System.out.println("LOGIN TIME : "+loginTime);
    }
  }
