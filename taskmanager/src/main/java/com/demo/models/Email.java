package com.demo.models;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class Email {


    private String recipient;

    private String sender;

    private String replyTo;

    private String subject;

    private String htmlBody;

    private String textBody;


}
