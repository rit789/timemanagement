package com.demo.models;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.util.Date;


@Entity
@Table(name = "otptransaction")
@Data
@Getter
@Setter
public class MgmtOtpTranaction {
  public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public String getOtpValue() {
		return otpValue;
	}

	public void setOtpValue(String otpValue) {
		this.otpValue = otpValue;
	}

	public Date getOtpCreationTime() {
		return otpCreationTime;
	}

	public void setOtpCreationTime(Date otpCreationTime) {
		this.otpCreationTime = otpCreationTime;
	}

	public Date getOtpExpiryTime() {
		return otpExpiryTime;
	}

	public void setOtpExpiryTime(Date otpExpiryTime) {
		this.otpExpiryTime = otpExpiryTime;
	}

	public int getOtpVerificationFailCount() {
		return otpVerificationFailCount;
	}

	public void setOtpVerificationFailCount(int otpVerificationFailCount) {
		this.otpVerificationFailCount = otpVerificationFailCount;
	}

	public int getStatus() {
		return status;
	}

	public void setStatus(int status) {
		this.status = status;
	}

	public String getMobileNo() {
		return mobileNo;
	}

	public void setMobileNo(String mobileNo) {
		this.mobileNo = mobileNo;
	}

@Id
  @GeneratedValue
  @Column(name = "id")
  private Integer id;

  @Column(name = "username")
  private String userName;

  @Column(name = "otpvalue")
  private String otpValue;

  @Column(name = "otpcreationtime")
  @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd HH:mm:ss.SSS", timezone = "IST")
  private Date otpCreationTime;

  @Column(name = "otpexpirytime")
  private Date otpExpiryTime;

  @Column(name = "otpverificationfailcount")
  private int otpVerificationFailCount;

  @Column(name = "status")
  private int status;

  @Column(name = "mobileno")
  private String mobileNo;

  public MgmtOtpTranaction() {

  }
}
