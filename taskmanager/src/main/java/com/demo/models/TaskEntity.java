package com.demo.models;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;


@Getter
@Setter
@Entity
@Table(name = "task")
public class TaskEntity {
  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  private int id;
  private String username;
  private String days;
  private String logInDate;
  private String logOutDate;
  private String logInTime;
  private String logOutTime;
  public int getId() {
	return id;
}
public void setId(int id) {
	this.id = id;
}
public String getUsername() {
	return username;
}
public void setUsername(String username) {
	this.username = username;
}
public String getDays() {
	return days;
}
public void setDays(String days) {
	this.days = days;
}
public String getLogInDate() {
	return logInDate;
}
public void setLogInDate(String logInDate) {
	this.logInDate = logInDate;
}
public String getLogOutDate() {
	return logOutDate;
}
public void setLogOutDate(String logOutDate) {
	this.logOutDate = logOutDate;
}
public String getLogInTime() {
	return logInTime;
}
public void setLogInTime(String logInTime) {
	this.logInTime = logInTime;
}
public String getLogOutTime() {
	return logOutTime;
}
public void setLogOutTime(String logOutTime) {
	this.logOutTime = logOutTime;
}
public String getSpendHrs() {
	return spendHrs;
}
public void setSpendHrs(String spendHrs) {
	this.spendHrs = spendHrs;
}
public String getLessHrs() {
	return lessHrs;
}
public void setLessHrs(String lessHrs) {
	this.lessHrs = lessHrs;
}
public String getExtraHrs() {
	return extraHrs;
}
public void setExtraHrs(String extraHrs) {
	this.extraHrs = extraHrs;
}
public String getProjectName() {
	return projectName;
}
public void setProjectName(String projectName) {
	this.projectName = projectName;
}
public String getRemarks() {
	return remarks;
}
public void setRemarks(String remarks) {
	this.remarks = remarks;
}
public String getModule() {
	return module;
}
public void setModule(String module) {
	this.module = module;
}
public String getMonth() {
	return month;
}
public void setMonth(String month) {
	this.month = month;
}
public int getYear() {
	return year;
}
public void setYear(int year) {
	this.year = year;
}
private String spendHrs;
  private String lessHrs;
  private String extraHrs;
  private String projectName;
  private String remarks;
  private String module;
  private String month;
  private int year;
}
