package com.demo.models;

import lombok.Getter;
import lombok.Setter;


@Getter
@Setter
public class Notification {
  private String birthday;
  private String holiday;
  public String getBirthday() {
	return birthday;
}
public void setBirthday(String birthday) {
	this.birthday = birthday;
}
public String getHoliday() {
	return holiday;
}
public void setHoliday(String holiday) {
	this.holiday = holiday;
}
public int getNotifyCount() {
	return notifyCount;
}
public void setNotifyCount(int notifyCount) {
	this.notifyCount = notifyCount;
}
private int notifyCount;
}
