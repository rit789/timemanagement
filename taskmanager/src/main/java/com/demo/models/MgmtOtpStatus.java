package com.demo.models;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Getter;
import lombok.Setter;


@Getter
@Setter
public class MgmtOtpStatus {
  public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public String getOtp() {
		return otp;
	}

	public void setOtp(String otp) {
		this.otp = otp;
	}

	public int getResendOTP() {
		return resendOTP;
	}

	public void setResendOTP(int resendOTP) {
		this.resendOTP = resendOTP;
	}

	public String getMobileNumber() {
		return mobileNumber;
	}

	public void setMobileNumber(String mobileNumber) {
		this.mobileNumber = mobileNumber;
	}

	public String getEmailAddress() {
		return emailAddress;
	}

	public void setEmailAddress(String emailAddress) {
		this.emailAddress = emailAddress;
	}

	public String getInternalResponseCode() {
		return internalResponseCode;
	}

	public void setInternalResponseCode(String internalResponseCode) {
		this.internalResponseCode = internalResponseCode;
	}

	public String getFailedVerificationCounter() {
		return failedVerificationCounter;
	}

	public void setFailedVerificationCounter(String failedVerificationCounter) {
		this.failedVerificationCounter = failedVerificationCounter;
	}

@JsonProperty("userName")
  private String userName = "";

  @JsonProperty("otp")
  private String otp = "";

  @JsonProperty("resendOTP")
  private int resendOTP = 0;

  @JsonProperty("mobileNumber")
  private String mobileNumber = "";

  @JsonProperty("emailAddress")
  private String emailAddress = "";

  @JsonProperty("internalResponseCode")
  private String internalResponseCode = "";

  @JsonProperty("failedVerificationCounter")
  private String failedVerificationCounter = "";
}
