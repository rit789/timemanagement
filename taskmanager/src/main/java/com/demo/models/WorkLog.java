package com.demo.models;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;


@Getter
@Setter
@Entity
@Table(	name = "worklog")
public class WorkLog {
  public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public String getUsername() {
		return username;
	}
	public void setUsername(String username) {
		this.username = username;
	}
	public String getDateStarted() {
		return dateStarted;
	}
	public void setDateStarted(String dateStarted) {
		this.dateStarted = dateStarted;
	}
	public String getProject() {
		return project;
	}
	public void setProject(String project) {
		this.project = project;
	}
	public String getJiraTask() {
		return jiraTask;
	}
	public void setJiraTask(String jiraTask) {
		this.jiraTask = jiraTask;
	}
	public String getJiraTicketNo() {
		return jiraTicketNo;
	}
	public void setJiraTicketNo(String jiraTicketNo) {
		this.jiraTicketNo = jiraTicketNo;
	}
	public String getTimeSpend() {
		return timeSpend;
	}
	public void setTimeSpend(String timeSpend) {
		this.timeSpend = timeSpend;
	}
	public String getWorkDescription() {
		return workDescription;
	}
	public void setWorkDescription(String workDescription) {
		this.workDescription = workDescription;
	}
	public String getMonth() {
		return month;
	}
	public void setMonth(String month) {
		this.month = month;
	}
@Id
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  private Long id;
  private String username;
  private String dateStarted;
  private String project;
  private String jiraTask;
  private String jiraTicketNo;
  private String timeSpend;
  private String workDescription;
  private String month;
}
