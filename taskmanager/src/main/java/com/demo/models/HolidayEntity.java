package com.demo.models;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;


@Getter
@Setter
@Entity
@Table(name = "holidays")
public class HolidayEntity {
  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  private int id;
  private String holiday;
  private String days;
  private String date;
  private String month;
  private int year;
}
