package com.demo.payload.response;

import lombok.Getter;
import lombok.Setter;


@Getter
@Setter
public class BillingEntity {
  public int getSr_NO() {
		return Sr_NO;
	}
	public void setSr_NO(int sr_NO) {
		Sr_NO = sr_NO;
	}
	public String getProject_Name() {
		return Project_Name;
	}
	public void setProject_Name(String project_Name) {
		Project_Name = project_Name;
	}
	public String getResource_Name() {
		return Resource_Name;
	}
	public void setResource_Name(String resource_Name) {
		Resource_Name = resource_Name;
	}
	public String getDate() {
		return Date;
	}
	public void setDate(String date) {
		Date = date;
	}
	public String getStart_Time() {
		return Start_Time;
	}
	public void setStart_Time(String start_Time) {
		Start_Time = start_Time;
	}
	public String getEnd_Time() {
		return End_Time;
	}
	public void setEnd_Time(String end_Time) {
		End_Time = end_Time;
	}
	public String getJIRA_No() {
		return JIRA_No;
	}
	public void setJIRA_No(String jIRA_No) {
		JIRA_No = jIRA_No;
	}
private int Sr_NO;
  private String Project_Name;
  private String Resource_Name;
  private String Date;
  private String Start_Time;
  private String End_Time;
  private String JIRA_No;
}
