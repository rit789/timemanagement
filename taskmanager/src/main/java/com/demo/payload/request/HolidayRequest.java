package com.demo.payload.request;

import lombok.Getter;
import lombok.Setter;


@Getter
@Setter
public class HolidayRequest {
  private String month;
  private int year;
public String getMonth() {
	return month;
}
public void setMonth(String month) {
	this.month = month;
}
public int getYear() {
	return year;
}
public void setYear(int year) {
	this.year = year;
}
}
