package com.demo;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;

import com.demo.repository.UserRepository;

@Configuration
@EnableAutoConfiguration
@SpringBootApplication
@EnableJpaRepositories(basePackageClasses = UserRepository.class)
public class TaskManagerApplication {
  @Autowired
  UserRepository userRepository;

  public static void main(String[] args) {
    SpringApplication.run(TaskManagerApplication.class, args);
  }
}
