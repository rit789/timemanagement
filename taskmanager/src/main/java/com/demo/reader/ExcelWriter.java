
package com.demo.reader;

import org.apache.poi.ss.usermodel.*;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.demo.models.TaskEntity;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.List;


public class ExcelWriter {
  private static final Logger LOGGER = LoggerFactory.getLogger(ExcelWriter.class);
  public static String buildExcel(List<TaskEntity> searchEntities){
    boolean isExits=false;
    String fileName="";
    byte[] contentReturn=null;
    try {
      String rootPath = System.getProperty("user.dir");
      File dir = new File(rootPath + "/" + "download");
      if (!dir.exists()) {
        dir.mkdir();
      }
      if(searchEntities!=null) {
        fileName = dir.getPath() + "/" + "timesheet" + ".xlsx";
        Workbook workbook = new XSSFWorkbook();
        Sheet sheet = workbook.createSheet("timesheet");
        sheet.setDefaultColumnWidth(30);
        sheet.autoSizeColumn(30);
        CellStyle style = workbook.createCellStyle();
        style.setFillForegroundColor(IndexedColors.YELLOW.getIndex());
        style.setFillPattern(CellStyle.SOLID_FOREGROUND);
        Font font = workbook.createFont();
        //font.setColor(IndexedColors.RED.getIndex());
        style.setFont(font);
        // create header row
        Row header = sheet.createRow(0);
        header.createCell(0).setCellValue("Days");
        header.getCell(0).setCellStyle(style);
        header.createCell(1).setCellValue("Login Date");
        header.getCell(1).setCellStyle(style);
        header.createCell(2).setCellValue("LogOut Date");
        header.getCell(2).setCellStyle(style);
        header.createCell(3).setCellValue("Login Time");
        header.getCell(3).setCellStyle(style);
        header.createCell(4).setCellValue("LogOut Time");
        header.getCell(4).setCellStyle(style);
        header.createCell(5).setCellValue("Spend Hours");
        header.getCell(5).setCellStyle(style);
        header.createCell(6).setCellValue("Project Name");
        header.getCell(6).setCellStyle(style);
        header.createCell(7).setCellValue("Module");
        header.getCell(7).setCellStyle(style);
        header.createCell(8).setCellValue("Remarks");
        header.getCell(8).setCellStyle(style);
        int rowCount = 1;
        for (TaskEntity task : searchEntities) {
          Row userRow = sheet.createRow(rowCount++);
            userRow.createCell(0).setCellValue(task.getDays() + " ");
            userRow.createCell(1).setCellValue(task.getLogInDate() + " ");
            userRow.createCell(2).setCellValue(task.getLogOutDate() + " ");
            userRow.createCell(3).setCellValue(task.getLogInTime() + " ");
            userRow.createCell(4).setCellValue(task.getLogOutTime() + " ");
            userRow.createCell(5).setCellValue(task.getSpendHrs() + " ");
            userRow.createCell(6).setCellValue(task.getProjectName() + " ");
            userRow.createCell(7).setCellValue(task.getModule() + " ");
            userRow.createCell(8).setCellValue(task.getRemarks() + " ");
          }
        workbook.write(new FileOutputStream(fileName));
      }
    } catch (IOException e) {
      e.printStackTrace();
    }
    System.out.println(fileName);
    return fileName;
  }

}

