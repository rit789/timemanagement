
package com.demo.repository;

import org.springframework.data.repository.CrudRepository;

import com.demo.models.MgmtOtpTranaction;


public interface OtpUserTransactionRepository extends CrudRepository<MgmtOtpTranaction, Integer> {
  /**
   *
   * @param userName
   * @return
   */
  MgmtOtpTranaction findOtpUserByUserName(String userName);
}
