package com.demo.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.demo.models.User;

import java.util.Optional;

@Repository
public interface UserRepository extends JpaRepository<User, Long> {

  Optional<User> findByUsername(String username);

  Boolean existsByUsername(String username);
  User findUserByUsername(String username);
  User findUserByEmail(String email);
  Boolean existsByEmail(String email);
  @Transactional
  @Query(value = "update users set password=?1 where username=?2 ", nativeQuery = true)
  String changePasswordByUsername(String password, String username);
  @Modifying
  @Query(value = "update users set firstname=?1,lastname=?2,email=?3,company=?4,designation=?5,manager=?6,manager_email=?7 where username=?8 ", nativeQuery = true)
  @Transactional
  boolean updateProfile(String firstname,String lastname,String email, String company,String designation, String manager,String manager_email, String username);



}
