package com.demo.repository;

import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;

import com.demo.models.ERole;
import com.demo.models.Role;


@Repository
public interface RoleRepository extends PagingAndSortingRepository<Role, Integer> {
Role findByName(ERole name);
}
