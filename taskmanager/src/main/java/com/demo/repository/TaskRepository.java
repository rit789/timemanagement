package com.demo.repository;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;

import com.demo.models.TaskEntity;
import com.demo.models.WorkLog;

import java.util.List;


@Repository
public interface TaskRepository extends PagingAndSortingRepository<TaskEntity, Integer> {
  Boolean existsByLogInDateAndUsername(String log_in_date,String username);
  List<TaskEntity> findByUsernameAndMonthAndYear(String username,String log_date,int year);

  @Query(value = "SELECT TIME_FORMAT(SEC_TO_TIME(SUM(TIME_TO_SEC(extra_hrs))),'%H:%i') FROM task where username=?1 and month=?2 and year=?3", nativeQuery = true)
  String extaHrsByUsername(String username,String month,int year);
  @Query(value = "SELECT TIME_FORMAT(SEC_TO_TIME(SUM(TIME_TO_SEC(less_hrs))),'%H:%i') FROM task where  username=?1 and month=?2 and year=?3", nativeQuery = true)
  String lessHrsByUsername(String username,String month,int year);

  @Query(value = "SELECT * FROM task where username =?1 and log_in_date between ?2 and ?3 and year=?4", nativeQuery = true)
  List<TaskEntity> findBillingDate(String username, String startDate, String EndDate,int year);
}
