package com.demo.repository;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;

import com.demo.models.WorkLog;

import java.util.List;


@Repository
public interface WorkLogRepository extends PagingAndSortingRepository<WorkLog, Integer> {
  List<WorkLog> findByUsernameAndMonth(String username,String log_date);
  WorkLog findWorkLogByUsernameAndDateStarted(String username,String date);
  @Query(value = "SELECT * FROM worklog where username =?1 and date_started between ?2 and ?3", nativeQuery = true)
  List<WorkLog> findWorkLogDate(String username,String startDate,String EndDate);
}
