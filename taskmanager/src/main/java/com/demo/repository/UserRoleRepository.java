package com.demo.repository;


import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;

import com.demo.models.User;

/**
 * Created by ext-parshotam on 15-11-2017.
 */
@Repository
public interface UserRoleRepository extends PagingAndSortingRepository<User, Integer> {
    //public User getByRoleName(String roleid);
}
