package com.demo.repository;

import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;

import com.demo.models.HolidayEntity;

import java.util.List;


@Repository
public interface HolidaysRepository extends PagingAndSortingRepository<HolidayEntity, Integer> {
  List<HolidayEntity> findByYear(int year);

}
