package com.demo.utils;

import com.demo.models.User;
import com.demo.repository.UserRepository;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AuthenticationProvider;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Component;


@Component
public class DbAuthenticate implements AuthenticationProvider {
  @Autowired
  UserRepository repository;
  @Autowired
  PasswordEncoder encoder;
  @Override
  public Authentication authenticate(Authentication authentication) throws AuthenticationException {


    if (authentication != null && authentication.getPrincipal() != null
        && authentication.getCredentials() != null) {

      User user = repository
          .findUserByUsername(authentication.getPrincipal().toString());

      if(user != null && user.getUsername()!= null)
      {
          if (user != null && user.getPassword() != null) {
            String password = user.getPassword();
            System.out.println(authentication.getCredentials());
            if(authentication.getCredentials().toString().equals(password))
            {
              return new UsernamePasswordAuthenticationToken(authentication.getPrincipal().toString(), authentication.getCredentials().toString(), null);
            }
            else
            {
              throw new BadCredentialsException("Incorrect Username/Password");
            }
        }
        else
        {
          throw new BadCredentialsException("Either is not of Member, Or User is Inactive");
        }
      }
      else
      {
        throw new BadCredentialsException("Incorrect Username/Password");

      }
    }

    return null;

  }

  @Override
  public boolean supports(Class<?> authentication) {
    // TODO Auto-generated method stub
    return true;
  }

}
