package com.demo.utils;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AuthenticationProvider;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Component;

import com.demo.repository.UserRepository;

import javax.naming.*;
import javax.naming.directory.*;
import java.util.Hashtable;


@Component
public class Ldap  implements AuthenticationProvider {
  @Autowired
  UserRepository repository;

  @Override
  public Authentication authenticate(Authentication authentication) {
    // TODO Auto-generated method stub
    boolean result = authenticate(authentication.getName(), authentication.getCredentials().toString());
    //boolean isUserAdded = repository.existsByUsername(authentication.getName());
    System.out.println("result " + result);
    if (result) {
      return new UsernamePasswordAuthenticationToken(authentication.getName(), authentication.getCredentials().toString(), null);
    } else {
      throw new BadCredentialsException("Username & Password invalid/Account is Inactive for LDAP");
    }
  }


  public static boolean authenticate(String name, String credential) {

    System.out.println("USER:>>>>>>>"+name);
    // TODO Auto-generated method stub
    //String name="subashc01487";
    Hashtable env = new Hashtable();
    env.put(Context.PROVIDER_URL, "ldap://mcx.in:389/dc=mcx,dc=in");
    env.put(Context.INITIAL_CONTEXT_FACTORY, "com.sun.jndi.ldap.LdapCtxFactory");
    env.put(Context.SECURITY_AUTHENTICATION, "simple");
    if(!name.contains("@mcx.in")){
      env.put(Context.SECURITY_PRINCIPAL, name + "@mcx.in");
    }

    env.put(Context.SECURITY_CREDENTIALS, credential);

    try {
      DirContext ctx = new InitialDirContext(env);
      System.out.println("connected");
//		        System.out.println(ctx.getEnvironment());

      // do something useful with the context...
      SearchControls controls = new SearchControls();
      controls.setSearchScope(SearchControls.SUBTREE_SCOPE);
      String[] attrIDs = {"cn", "department", "userPrincipalName", "mail", "sAMAccountType", "sAMAccountName","dob"};
      controls.setReturningAttributes(attrIDs);
      NamingEnumeration<SearchResult> search = ctx.search("", "(&(objectCategory=person)(objectClass=user)(sAMAccountName=" + name + "))", controls);
      // System.out.println(search);
      if (search.hasMore()) {
        //List of attributes found in LDAP for given user
        Attributes attributes = search.next().getAttributes();
        System.out.println(attributes.get(attrIDs[0]));
        System.out.println(attributes.get(attrIDs[1]));
        System.out.println(attributes.get(attrIDs[2]));
        System.out.println(attributes.get(attrIDs[3]));
        System.out.println(attributes.get(attrIDs[4]));
        System.out.println(attributes.get(attrIDs[5]));
        System.out.println(attributes.get(attrIDs[6]));

      }
      ctx.close();
      return true;
    } catch (AuthenticationNotSupportedException ex) {
      System.out.println("The authentication is not supported by the server");
      return false;
    } catch (AuthenticationException ex) {
      System.out.println("incorrect password or username");
      return false;
    } catch (NamingException ex) {
      ex.printStackTrace();
      System.out.println("error when trying to create the context");
      return false;
    }
  }
  @Override
  public boolean supports(Class<?> authentication) {
    // TODO Auto-generated method stub
    return true;
  }
}