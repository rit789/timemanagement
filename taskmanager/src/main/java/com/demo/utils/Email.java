package com.demo.utils;

import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;

import com.demo.models.TaskEntity;
import com.demo.models.WorkLog;

import java.io.File;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.List;
import java.util.Properties;


public class Email {
  public static void main(String [] args) {
    String recipient = "";

    // email ID of Sender.
    String sender = "Warehousing Inspection <No-Reply@mcxccl.com>";
    sendMail("abc",recipient,recipient,recipient,"test","testing","abc");
  }
  public static void  sendMail(String senderName,String from, String to, String cc, String subject,String body,String manager){

    // using host as localhost
    String host = "mail.mcxindia.com";

    // Getting system properties
    Properties properties = System.getProperties();

    // Setting up mail server
    properties.setProperty("mail.smtp.host", host);

    // creating session object to get properties
    Session session = Session.getDefaultInstance(properties);

    try
    {
      // MimeMessage object.
      MimeMessage message = new MimeMessage(session);

      // Set From Field: adding senders email to from field.
      message.setFrom(new InternetAddress(senderName+"<"+from+">"));


      // Set To Field: adding recipient's email to from field.
      message.addRecipient(Message.RecipientType.TO, new InternetAddress(to));
      if(!cc.isEmpty() && cc!=null) {
        message.addRecipient(Message.RecipientType.CC, new InternetAddress(cc));
      }

      // Set Subject: subject of the email
      message.setSubject(subject);

      // set body of the email.
      message.setText("Hi, "+manager+"\n\n"+body);

      // Send email.
      Transport.send(message);
      System.out.println("Mail successfully sent");
    }
    catch (MessagingException mex)
    {
      mex.printStackTrace();
    }
  }
  public static void sendLogMail(String senderName, String from, String to,String subject, List<WorkLog> workLogList ,String start,String end,String manager,String month,String name) {
    String host = "mail.mcxindia.com";
    Properties properties = System.getProperties();
    properties.setProperty("mail.smtp.host", host);
    Session session = Session.getDefaultInstance(properties);
    File dir = new File(System.getProperty("user.dir") + "/" + "image");
    if(!dir.exists()){
      dir.mkdir();
    }
    String imgPath = System.getProperty("user.dir") + "/" + "image/logo.jpg";
    try {
      MimeMessage message = new MimeMessage(session);
      message.setFrom(new InternetAddress(senderName + "<" + from + ">"));
      message.addRecipient(Message.RecipientType.TO, new InternetAddress(to));
      message.setSubject(subject);
      String text="";
      String period ="";

      if(start !=null && end !=null){
        period = start.concat(" to ").concat(end);
      }
      else {
        period = month;
      }
      text=
          "<head><link rel=\"stylesheet\" href=\"https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css\"></head>"+
          "<b>Hi,</b> "+manager+"\n\n" +
              "<br>"+
              "<br>"+
              "Work Log Period - "+period+""+
              "<br>"+
              "<br>"+
              "<table border=\"1\" cellpadding=\"0\" cellspacing=\"0\" width=\"100%\" style=\"border-collapse: collapse;\">"
              + "<tr align='center'>"
              + "<td width=\"10%\"><b>Date<b></td>"
              + "<td><b>Project Name<b></td>"
              + "<td><b>Jira Task<b></td>"
              + "<td><b>Jira Ticket No<b></td>"
              + "<td><b>Time Spent<b></td>"
              + "<td><b>Work Description<b></td>"
              + "</tr>";
      for (WorkLog workLog : workLogList) {
        text=text+"<tr align='center'>"
            +"<td>" +  workLog.getDateStarted() + "</td>"
            +"<td>" +  workLog.getProject() + "</td>"
            + "<td>" + workLog.getJiraTask() + "</td>"
            + "<td>" + workLog.getJiraTicketNo() + "</td>"
            + "<td>" + workLog.getTimeSpend() + "</td>"
            + "<td>" + workLog.getWorkDescription() + "</td>"
            +"</tr>";

      }
     text=text+
         "</table>"+
         "<br>"+
         "<br>"+
         "<br>"+
         "<br>"+
         "<p style=\"color:#15317E\">"+
          "<b style=\"font-size: 24px\">"+name+"</b><br>" +
          "Multi Commodity Exchange of India Ltd.<br>" +
          "Exchange Square, Suren Road, Chakala <br>" +
          "Andheri (East), Mumbai – 400093, India<br>" +
          "Phone: +91-22-67318888 <br>" +
          "Email:"+from+"<br><br>"+
          "<img src="+imgPath+">"
          +"</p>";
      message.setContent(text, "text/html; charset=utf-8");
      Transport.send(message);

    } catch (MessagingException mex) {
      mex.printStackTrace();
    }
  }
  public static void sendBillingMail(String senderName, String from, String to,String subject, List<TaskEntity> workLogList ,String start,String end,String manager,String month,String name,int year) {
    String host = "mail.mcxindia.com";
    Properties properties = System.getProperties();
    properties.setProperty("mail.smtp.host", host);
    Session session = Session.getDefaultInstance(properties);
    File dir = new File(System.getProperty("user.dir") + "/" + "image");
    if(!dir.exists()){
      dir.mkdir();
    }
    String imgPath = System.getProperty("user.dir") + "/" + "image/logo.jpg";
    try {
      MimeMessage message = new MimeMessage(session);
      message.setFrom(new InternetAddress(senderName.toUpperCase() + "<" + from + ">"));
      message.addRecipient(Message.RecipientType.TO, new InternetAddress(to));
      message.setSubject(subject);
      String text="";
      String period ="";
      DateFormat dateFormat = new SimpleDateFormat("dd-MM-yyyy");
      int srNo = 1;
      if(start !=null && end !=null){
        period = start.concat(" to ").concat(end);
      }
      else {
        period = month +"-"+year;
      }
      text=
          "<head><link rel=\"stylesheet\" href=\"https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css\"></head>"+
              "<b>Hi,</b> "+manager+"\n\n" +
              "<br>"+
              "<br>"+
              "Consultant Billing - "+period+" Onwards"+
              "<br>"+
              "<br>"+
              "<table border=\"1\" cellpadding=\"0\" cellspacing=\"0\" width=\"60%\" style=\"border-collapse: collapse;\">"
              + "<tr align='center' style='background-color:#57B3D9'>"
              + "<td width=\"4%\"><b>Sr. No<b></td>"
              + "<td><b>Resource name<b></td>"
              + "<td><b>Project Name<b></td>"
              + "<td><b>Date<b></td>"
              + "<td><b>Start Time<b></td>"
              + "<td><b>End Time<b></td>"
              + "<td><b>JIRA NO<b></td>"
              + "</tr>";
      for (TaskEntity workLog : workLogList) {
        text=text+"<tr align='center'>"
            +"<td>" +  srNo++ + "</td>"
            +"<td>" +  name + "</td>"
            +"<td>" +  workLog.getProjectName() + "</td>"
            +"<td>" +  workLog.getLogInDate() + "</td>"
            + "<td>" + workLog.getLogInTime() + "</td>"
            + "<td>" + workLog.getLogOutTime() + "</td>"
            + "<td>" + workLog.getModule() + "</td>"
            +"</tr>";

      }
      text=text+
          "</table>"+
          "<br>"+
          "<br>"+
          "<br>"+
          "<br>"+
          "<p style=\"color:#15317E\">"+
          "<b style=\"font-size: 24px\">"+name+"</b><br>" +
          "Multi Commodity Exchange of India Ltd.<br>" +
          "Exchange Square, Suren Road, Chakala <br>" +
          "Andheri (East), Mumbai – 400093, India<br>" +
          "Phone: +91-22-67318888 <br>" +
          "Email:"+from+"<br><br>"+
          "<img src="+imgPath+">"
          +"</p>";
      message.setContent(text, "text/html; charset=utf-8");
      Transport.send(message);

    } catch (MessagingException mex) {
      mex.printStackTrace();
    }
  }
}
