package com.demo.service;

import com.demo.models.HolidayEntity;
import com.demo.models.TaskEntity;
import com.demo.models.WorkLog;
import com.demo.repository.HolidaysRepository;
import com.demo.repository.TaskRepository;
import com.demo.repository.WorkLogRepository;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.stream.Collectors;


@Service
public class TaskService {
  @Autowired
  TaskRepository repository;
  @Autowired
  HolidaysRepository holidaysRepository;
  @Autowired
  WorkLogRepository workLogRepository;
  public void createTask(TaskEntity taskEntity){
    if(repository.existsById(taskEntity.getId())){
      repository.save(taskEntity);
    }else {
      repository.save(taskEntity);
    }
  }
  public boolean existsByUsername(String log_in_date,String username){
    return repository.existsByLogInDateAndUsername(log_in_date,username);
  }
  public List<TaskEntity> getTask(String username,String month,int year){
    return repository.findByUsernameAndMonthAndYear(username,month,year);
  }

  public List<TaskEntity> getBillingLogDate(String username,String startDate,String endDate,int year){
    return repository.findBillingDate(username,startDate,endDate,year);
  }

  public List<HolidayEntity> getHolidays(int year){
    List<HolidayEntity> list = holidaysRepository.findByYear(year)
        .stream()
        .collect(Collectors.toList());
    return list;
  }
  public String getExtrahrs(String username, String month, int year, String day) {
    return repository.extaHrsByUsername(username, month, year);
  }
  public String getLessHrs(String username, String month, int year, String day) {
    return repository.lessHrsByUsername(username, month, year);
  }
  @Transactional
  public void workLog(WorkLog workLog){

    workLogRepository.save(workLog);
   // }
  }
  public List<WorkLog> getWorkLogs(String username,String month){
    return workLogRepository.findByUsernameAndMonth(username,month);
  }
  public List<WorkLog> getWorkLogDate(String username,String startDate,String endDate){
    return workLogRepository.findWorkLogDate(username,startDate,endDate);
  }
  public WorkLog workLog(String username,String date){
    return workLogRepository.findWorkLogByUsernameAndDateStarted(username,date);
  }

}
