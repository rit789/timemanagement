package com.demo.service;

import com.demo.models.MgmtOtpStatus;
import com.demo.models.MgmtOtpTranaction;
import com.demo.repository.OtpUserTransactionRepository;
import com.demo.utils.Email;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.MessageSource;
import org.springframework.stereotype.Service;

import java.io.Serializable;
import java.text.SimpleDateFormat;
import java.util.*;



@Service
public class OtpTransactionService implements Serializable {

  private static final Logger LOGGER = LoggerFactory.getLogger(OtpTransactionService.class);
  @Value("${max.validation.limit}")
  private static final int MAX_VALIDATION_LIMIT = 5;
  @Value("${thirty.minute.in.msec}")
  private final String THIRTY_MINUTE_IN_MILLISEC = "1800000";

  @Value("${otp.life.time}")
  private final String OTP_LIFE_TIME = "1800";
  private final transient MessageSource messageResource;
  SimpleDateFormat format = new SimpleDateFormat("MM/dd/yyyy HH:mm:ss");
  @Autowired
  OtpUserTransactionRepository otpUserTransactionRepository;


  public OtpTransactionService(MessageSource messageResource) {
    this.messageResource = messageResource;
  }
    /*
    internalErrorCode = 4
    internalErrorCode
    1 = success
    2 = Wrong OTP value
    3 = Maximum attempt reached
    4 = Otp has Expired
     */

  public static char[] OTP(int len) {
    // Using numeric values
    String numbers = "0123456789";
    // Using random method
    Random rndm_method = new Random();
    char[] otp = new char[len];
    for (int i = 0; i < len; i++) {
      // Use of charAt() method : to get character value
      // Use of nextInt() as it is scanning the value as int
      otp[i] =
          numbers.charAt(rndm_method.nextInt(numbers.length()));
    }
    return otp;
  }

  public int validateOtp(String userName, String inParamOtp) {
    MgmtOtpTranaction otpUser = findExistingUser(userName);

    if (otpUser != null) {
      String dbOtp = otpUser.getOtpValue();

      Date otpExpDate = otpUser.getOtpExpiryTime();
      long secsOtpExpDate = otpExpDate.getTime() / 1000;

      Date curDate = new Date();
      long secsCurDate = curDate.getTime() / 1000;

      if (dbOtp.equalsIgnoreCase(inParamOtp)) {
        if (otpUser.getOtpVerificationFailCount() <= MAX_VALIDATION_LIMIT) {
          if (secsOtpExpDate - secsCurDate < Long.parseLong(OTP_LIFE_TIME)) {
            if (secsOtpExpDate > secsCurDate) {
              resetFailCounter(otpUser);
              return 1;   //successful
            } else {
              increaseFailCounter(otpUser);
              return 4; //Otp has Expired
            }
          } else {
            increaseFailCounter(otpUser);
            return 4; //Otp has Expired
          }
        } else {
          increaseFailCounter(otpUser);
          return 3;//Maximum attempt reached
        }
      } else {
        increaseFailCounter(otpUser);
        return 2; //Wrong OTP value
      }
    }

    return 5;
  }

  private void increaseFailCounter(MgmtOtpTranaction otpUser) {
    if (null != otpUser) {
      otpUser.setOtpVerificationFailCount(otpUser.getOtpVerificationFailCount() + 1);
      otpUserTransactionRepository.save(otpUser);
    }
  }

  private void resetFailCounter(MgmtOtpTranaction otpUser) {
    if (null != otpUser) {
      otpUser.setOtpVerificationFailCount(0);
      otpUserTransactionRepository.save(otpUser);
    }
  }

  public boolean sendOTP(MgmtOtpStatus mgmtOtpStatus, String msg) {

    MgmtOtpTranaction otpUser = findExistingUser(mgmtOtpStatus.getEmailAddress());
    String otpValue = String.valueOf(OTP(6));
    Date creationDate = new Date(System.currentTimeMillis());
    Date expityDate = new Date(System.currentTimeMillis() + Long.parseLong(THIRTY_MINUTE_IN_MILLISEC));

    if (otpUser != null) { //User entry is present in table, update the Otp

      otpUser.setUserName(mgmtOtpStatus.getEmailAddress());
      otpUser.setStatus(1);
      otpUser.setOtpVerificationFailCount(0);
      otpUser.setOtpValue(otpValue);
      otpUser.setOtpCreationTime(creationDate);
      otpUser.setOtpExpiryTime(expityDate);

      otpUserTransactionRepository.save(otpUser);

    } else {//User is not present in table, insert user and Otp details

      otpUser = new MgmtOtpTranaction();
      otpUser.setUserName(mgmtOtpStatus.getEmailAddress());
      otpUser.setStatus(1);
      otpUser.setOtpVerificationFailCount(0);
      otpUser.setOtpValue(otpValue);
      otpUser.setOtpCreationTime(creationDate);
      otpUser.setOtpExpiryTime(expityDate);
      otpUser.setMobileNo(String.valueOf(mgmtOtpStatus.getMobileNumber()));

      otpUserTransactionRepository.save(otpUser);
    }

    sendSMS(mgmtOtpStatus, otpValue,msg);

    return true;
  }

  public MgmtOtpTranaction findExistingUser(String userName) {
    return otpUserTransactionRepository.findOtpUserByUserName(userName);
  }

  private void sendSMS(MgmtOtpStatus user, String otpValue, String msg) {
    System.out.println(msg);
    Email.sendMail(user.getUserName() ,user.getEmailAddress(),user.getEmailAddress(),"","OTP to Reset Password","System Generated OTP is ".concat(otpValue)+"\n\n\n\n"+msg,user.getUserName().toUpperCase());
    //sendEmail(user.getUserName(),user.getEmailAddress(), otpValue);
  }

}
