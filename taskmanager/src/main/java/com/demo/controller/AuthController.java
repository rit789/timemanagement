package com.demo.controller;

import com.demo.models.ERole;
import com.demo.models.MgmtOtpStatus;
import com.demo.models.Role;
import com.demo.models.User;
import com.demo.payload.request.LoginRequest;
import com.demo.payload.request.PasswordRequest;
import com.demo.payload.request.SignupRequest;
import com.demo.payload.response.JwtResponse;
import com.demo.payload.response.MessageResponse;
import com.demo.repository.OtpUserTransactionRepository;
import com.demo.repository.RoleRepository;
import com.demo.repository.UserRepository;
import com.demo.security.jwt.JwtTokenUtil;
import com.demo.security.jwt.JwtUtils;
import com.demo.service.OtpTransactionService;
import com.demo.utils.Email;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.HashSet;
import java.util.Set;
import java.util.logging.Logger;
@CrossOrigin(origins = "*", maxAge = 3600)
@RestController
@RequestMapping("/api/auth")
public class AuthController {
  private final static Logger LOGGER =
      Logger.getLogger(Logger.GLOBAL_LOGGER_NAME);
  @Autowired
  AuthenticationManager authenticationManager;
  @Autowired
  UserRepository userRepository;
  @Autowired
  RoleRepository roleRepository;
  @Autowired
  OtpUserTransactionRepository otpRepository;
  @Autowired
  PasswordEncoder encoder;
  @Autowired
  OtpTransactionService otpTransactionService;
  @Autowired
  JwtUtils jwtUtils;
  @Autowired
  private JwtTokenUtil jwtTokenUtil;
  @Value("${server.url}")
  private String url;
  @Value("${app-name}")
  private String appName;
  @Value("${login.otp.expired.message}")
  private String expiredMsg;
  @PostMapping("/signin")

  public ResponseEntity<?> authenticateUser(@Valid @RequestBody LoginRequest loginRequest) {
    LOGGER.info("Authencation");
    String jwt = null;
    ResponseEntity<JwtResponse> responseEntity =null;

    UsernamePasswordAuthenticationToken authenticationToken = new UsernamePasswordAuthenticationToken(
        loginRequest.getUsername(), loginRequest.getPassword());

    authenticationToken.setDetails(loginRequest);
    Authentication retAuth = authenticationManager.authenticate(authenticationToken);
    System.out.println("Authentication " + retAuth.isAuthenticated());

    if(retAuth.isAuthenticated()) {
      String token = jwtTokenUtil.generateToken(loginRequest.getUsername());
      //User user = userRepository.findUserByUsername(loginRequest.getUsername());
      responseEntity = ResponseEntity
          .ok(new JwtResponse(token,
              loginRequest.getUsername()));

    }else {
      return ResponseEntity
          .badRequest()
          .body(new MessageResponse("Error: Email is already in use!"));
    }
//    }else {
//
//      Authentication authentication = authenticationManager.authenticate(
//          new UsernamePasswordAuthenticationToken(loginRequest.getUsername(), loginRequest.getPassword()));
//
//      SecurityContextHolder.getContext().setAuthentication(authentication);
//
//      jwt = jwtUtils.generateJwtToken(authentication);
//
//      UserDetailsImpl userDetails = (UserDetailsImpl) authentication.getPrincipal();
//      List<String> roles = userDetails.getAuthorities().stream()
//          .map(item -> item.getAuthority())
//          .collect(Collectors.toList());
//
//      return ResponseEntity.ok(new JwtResponse(jwt,
//          userDetails.getId(),
//          userDetails.getUsername(),
//          userDetails.getEmail(),
//          userDetails.getManager(),
//          userDetails.getManagerEmail(),
//          userDetails.getFirstname(),
//          userDetails.getLastname(),
//          userDetails.getCompany(),
//          roles));
//    }
    return responseEntity;
  }
  @PostMapping("/changePassword")
  public ResponseEntity<?> changePassword(@RequestBody @Valid PasswordRequest passwordRequest) {
    MessageResponse msg= null;
    if (userRepository.existsByEmail(passwordRequest.getEmail())) {
      int i = otpTransactionService.validateOtp(passwordRequest.getEmail(), passwordRequest.getOtp());
      if(i==1) {

        User user = new User();
        User userDetails = userRepository.findUserByEmail(passwordRequest.getEmail());
        if (userDetails!=null) {
          user.setId(userDetails.getId());
          user.setUsername(userDetails.getUsername());
          user.setFirstname(userDetails.getFirstname());
          user.setLastname(userDetails.getLastname());
          user.setEmail(userDetails.getEmail());
          user.setCompany(userDetails.getCompany());
          user.setDesignation(userDetails.getDesignation());
          user.setManager(userDetails.getManager());
          user.setManagerEmail(userDetails.getManagerEmail());
          user.setPassword(passwordRequest.getPassword());
          User save = userRepository.save(user);
          msg= (new MessageResponse("password changed successfully!"));
        }
        if(user!=null){
          Email.sendMail(
              userDetails.getUsername(),
              userDetails.getEmail(),
              userDetails.getEmail(),"",
              "New Username & Password",appName+"-"+url+"\n\n"
                  +"Username: "+userDetails.getUsername()+"\n\n"
                  +"Password: "+  passwordRequest.getPassword(),
              userDetails.getFirstname());
        }

      }else if(i==2){
        msg= (new MessageResponse("wrong otp!"));
      }
    }else {
      msg= (new MessageResponse("otp expired!"));
    }
    return ResponseEntity.ok(msg);
  }
@Transactional
  @PostMapping("/update-profile")
  public ResponseEntity<?> updateProfile(@RequestBody @Valid SignupRequest signupRequest) {
    MessageResponse msg= null;
    User user = new User();
    User userDetails = userRepository.findUserByUsername(signupRequest.getUsername());
    if (userDetails!=null) {
      user.setId(userDetails.getId());
      user.setUsername(userDetails.getUsername());
      user.setFirstname(signupRequest.getFirstname());
      user.setLastname(signupRequest.getLastname());
      user.setDob(signupRequest.getDob());
      user.setEmail(signupRequest.getEmail());
      user.setCompany(signupRequest.getCompany());
      user.setDesignation(signupRequest.getDesignation());
      user.setManager(signupRequest.getManager());
      user.setManagerEmail(signupRequest.getManagerEmail());
      user.setPassword(userDetails.getPassword());
      userRepository.save(user);
      msg = (new MessageResponse("profile updated successfully!"));
    }else{
      user.setUsername(signupRequest.getUsername());
      user.setFirstname(signupRequest.getFirstname());
      user.setLastname(signupRequest.getLastname());
      user.setDob(signupRequest.getDob());
      user.setEmail(signupRequest.getEmail());
      user.setCompany(signupRequest.getCompany());
      user.setDesignation(signupRequest.getDesignation());
      user.setManager(signupRequest.getManager());
      user.setManagerEmail(signupRequest.getManagerEmail());
      user.setPassword("");
      userRepository.save(user);
      msg = (new MessageResponse("profile updated successfully!"));
    }
    //boolean profile = userRepository.updateProfile(signupRequest.getFirstname(),signupRequest.getLastname(),signupRequest.getEmail(), signupRequest.getCompany(), signupRequest.getDesignation(), signupRequest.getManager(), signupRequest.getManagerEmail(), signupRequest.getUsername());
    //if(profile) {

    //}
    //}else {
    //msg= (new MessageResponse("User not updated!"));
    //}
    return ResponseEntity.ok(msg);
  }
  @PostMapping("/getOtp")
  public ResponseEntity<?> getOpt(@RequestBody @Valid PasswordRequest passwordRequest) {
    MessageResponse msg= null;
    if (userRepository.existsByEmail(passwordRequest.getEmail())) {
      User user = userRepository.findUserByEmail(passwordRequest.getEmail());
      MgmtOtpStatus mgmtOtpStatus = new MgmtOtpStatus();
      mgmtOtpStatus.setUserName(user.getUsername());
      mgmtOtpStatus.setEmailAddress(user.getEmail());
      boolean otp = otpTransactionService.sendOTP(mgmtOtpStatus,expiredMsg);
      if(otp){
        msg= (new MessageResponse("otp sended successfully!"));
      }
    }

    return ResponseEntity.ok(msg);
  }
  @PostMapping("/profile")
  public ResponseEntity<?> getProfile(@RequestBody @Valid SignupRequest signupRequest) {
    User user  = new User();
    User userlist = userRepository.findUserByUsername(signupRequest.getUsername());
    if(userlist!=null){
      user.setId(userlist.getId());
      user.setUsername(userlist.getUsername());
      user.setFirstname(userlist.getFirstname());
      user.setLastname(userlist.getLastname());
      user.setDob(userlist.getDob());
      user.setEmail(userlist.getEmail());
      user.setCompany(userlist.getCompany());
      user.setDesignation(userlist.getDesignation());
      user.setManager(userlist.getManager());
      user.setManagerEmail(userlist.getManagerEmail());
    }
    return new ResponseEntity(user, HttpStatus.ACCEPTED);
  }
  @PostMapping("/signup")
  public ResponseEntity<?> registerUser(@Valid @RequestBody SignupRequest signUpRequest) {
    try {


      if (userRepository.existsByUsername(signUpRequest.getUsername())) {
        return ResponseEntity
            .badRequest()
            .body(new MessageResponse("Error: Username is already taken!"));
      }

      if (userRepository.existsByEmail(signUpRequest.getEmail())) {
        return ResponseEntity
            .badRequest()
            .body(new MessageResponse("Error: Email is already in use!"));
      }

      // Create new user's account
      User user = new User(signUpRequest.getUsername(),signUpRequest.getPassword(),signUpRequest.getFirstname(),
          signUpRequest.getLastname(),signUpRequest.getDob(),signUpRequest.getEmail(),signUpRequest.getCompany(),
          signUpRequest.getDesignation(),signUpRequest.getManager(),signUpRequest.getManagerEmail()
      );

      Set<String> strRoles = signUpRequest.getRole();
      Set<Role> roles = new HashSet<>();

      if (strRoles == null) {
        Role userRole = roleRepository.findByName(ERole.ROLE_USER);
        roles.add(userRole);
      } else {
        strRoles.forEach(role -> {
          switch (role) {
            case "admin":
              Role adminRole = roleRepository.findByName(ERole.ROLE_ADMIN);
              roles.add(adminRole);

              break;
            case "mod":
              Role modRole = roleRepository.findByName(ERole.ROLE_MODERATOR);
              roles.add(modRole);

              break;
            default:
              Role userRole = roleRepository.findByName(ERole.ROLE_USER);
              roles.add(userRole);
          }
        });
      }

      user.setRoles(roles);
      User save = userRepository.save(user);
      if(save!=null){
        Email.sendMail(
            save.getUsername(),save.getEmail(),save.getEmail(),"","Username & Credentials",appName+"-"+url+"\n\n"+"Username: "+save.getUsername()+"\n\n"+"Password: "+signUpRequest.getPassword(),save.getFirstname());
      }
    }catch (Exception e){
      e.printStackTrace();
    }
    return ResponseEntity.ok(new MessageResponse("User registered successfully!"));
  }
}