package com.demo.controller;

import com.demo.models.HolidayEntity;
import com.demo.models.TaskEntity;
import com.demo.models.User;
import com.demo.models.WorkLog;
import com.demo.payload.request.HolidayRequest;
import com.demo.payload.request.LeaveRequest;
import com.demo.payload.request.TaskRequest;
import com.demo.payload.request.WorkLogRequest;
import com.demo.payload.response.BillingEntity;
import com.demo.payload.response.MessageResponse;
import com.demo.repository.UserRepository;
import com.demo.service.TaskService;
import com.demo.utils.Email;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.text.DateFormat;
import java.text.DateFormatSymbols;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.util.*;
import java.util.concurrent.TimeUnit;
import java.util.stream.Collectors;


@CrossOrigin(origins = "*", maxAge = 3600)
@RestController
@RequestMapping("/api/test")
public class TestController {
  @Autowired
  TaskService service;
  @Autowired
  UserRepository userRepository;
  @GetMapping("/all")
  public String allAccess() {
    return "Public Content.";
  }

  @GetMapping("/user")
  @PreAuthorize("hasRole('USER') or hasRole('MODERATOR') or hasRole('ADMIN')")
  public String userAccess() {
    return "User Content.";
  }

  @GetMapping("/mod")
  @PreAuthorize("hasRole('MODERATOR')")
  public String moderatorAccess() {
    return "Moderator Board.";
  }

  @GetMapping("/admin")
  @PreAuthorize("hasRole('ADMIN')")
  public String adminAccess() {
    return "Admin Board.";
  }

  @PostMapping("/create")
  public ResponseEntity<?> taskCreate(@RequestBody @Valid TaskRequest taskRequest) throws ParseException {
    TaskEntity taskEntity = new TaskEntity();
    LocalDate today = LocalDate.now();
    SimpleDateFormat localDateFormat = new SimpleDateFormat("HH:mm");
    localDateFormat.setTimeZone(TimeZone.getTimeZone("Asia/Kolkata"));
    String time = localDateFormat.format(new Date());
    DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
    dateFormat.setTimeZone(TimeZone.getTimeZone("Asia/Kolkata"));
    String log_in = dateFormat.format(new Date());
    LocalDate dt = LocalDate.parse(log_in);

    if (service.existsByUsername(log_in,taskRequest.getUsername())) {
      return ResponseEntity
          .badRequest()
          .body(new MessageResponse("Error: Username Task is already taken!"));
    }else {
      DateFormat dateFromat2 = new SimpleDateFormat("EEEE");
      String finalDay = dateFromat2.format(dateFormat.parse(log_in));
      Date date1 = new SimpleDateFormat("HH:mm").parse(time);
      Date date2 = new SimpleDateFormat("HH:mm").parse("19:00");
      long diff = date2.getTime() - date1.getTime();
      int year = today.getYear();
      Date spend = new SimpleDateFormat("HH:mm").parse(formatDuration(diff));
      Date total = new SimpleDateFormat("HH:mm").parse("09:00");
      long calHrs = spend.getTime() - total.getTime();
      taskEntity.setDays(finalDay);
      taskEntity.setUsername(taskRequest.getUsername());
      taskEntity.setLogInDate(log_in);
      taskEntity.setLogInTime(time);
      taskEntity.setSpendHrs(formatDuration(diff));
      if(formatDuration(calHrs).contains("-")){
        taskEntity.setLessHrs((formatDuration(calHrs)).replace("-",""));
        taskEntity.setExtraHrs(("00:00"));
      }
      else {
        taskEntity.setExtraHrs((formatDuration(calHrs)));
        taskEntity.setLessHrs(("00:00"));
      }
      taskEntity.setMonth(dt.getMonth().name());
      taskEntity.setYear(year);
      taskEntity.setLogOutDate(log_in);
      taskEntity.setLogOutTime("19:00");
      taskEntity.setProjectName(taskRequest.getProjectName());
      taskEntity.setModule(taskRequest.getModule());
      taskEntity.setRemarks(taskRequest.getRemarks());

      service.createTask(taskEntity);
      return ResponseEntity.ok(new MessageResponse("Task created successfully!"));
    }
  }
  @PostMapping("/manual")
  public ResponseEntity<?> manualPunch(@RequestBody @Valid TaskRequest taskRequest) throws ParseException {
    TaskEntity taskEntity = new TaskEntity();
    LocalDate today = LocalDate.parse(taskRequest.getLogInDate());
    DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
    dateFormat.setTimeZone(TimeZone.getTimeZone("Asia/Kolkata"));
    String msg= null;
    LocalDate dt = LocalDate.parse(taskRequest.getLogInDate());
    try {
      if (service.existsByUsername(taskRequest.getLogInDate(),taskRequest.getUsername())) {
        return ResponseEntity
            .badRequest()
            .body(new MessageResponse("Error: Username Task is already taken!"));
      }else {
        DateFormat dateFromat2 = new SimpleDateFormat("EEEE");
        String finalDay = dateFromat2.format(dateFormat.parse(taskRequest.getLogInDate()));
        Date date1 = new SimpleDateFormat("HH:mm").parse(taskRequest.getLogInTime());
        Date date2 = new SimpleDateFormat("HH:mm").parse(taskRequest.getLogOutTime());
        long diff = date2.getTime() - date1.getTime();
        int year = today.getYear();
        Date spend = new SimpleDateFormat("HH:mm").parse(formatDuration(diff));
        Date total = new SimpleDateFormat("HH:mm").parse("09:00");
        long calHrs = spend.getTime() - total.getTime();

        taskEntity.setDays(finalDay);
        taskEntity.setUsername(taskRequest.getUsername());
        taskEntity.setLogInDate(taskRequest.getLogInDate());
        taskEntity.setLogInTime(taskRequest.getLogInTime());
        taskEntity.setSpendHrs(formatDuration(diff));
        if(formatDuration(calHrs).contains("-")){
          taskEntity.setLessHrs((formatDuration(calHrs)).replace("-",""));
          taskEntity.setExtraHrs(("00:00"));
        }
        else {
          taskEntity.setExtraHrs((formatDuration(calHrs)));
          taskEntity.setLessHrs(("00:00"));
        }
        taskEntity.setMonth(dt.getMonth().name());
        taskEntity.setYear(year);
        taskEntity.setLogOutDate(taskRequest.getLogOutDate());
        taskEntity.setLogOutTime(taskRequest.getLogOutTime());
        taskEntity.setProjectName(taskRequest.getProjectName());
        taskEntity.setModule(taskRequest.getModule());
        taskEntity.setRemarks(taskRequest.getRemarks());
        Date enteredDate = null;
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
        enteredDate = sdf.parse(taskRequest.getLogInDate());
        Date currentDate = new Date();
        if (enteredDate.after(currentDate)) {
          msg="future date not allow";
        } else{

          msg ="task created succesfully";
          service.createTask(taskEntity);
        }
      }
    }catch (Exception e){
      e.printStackTrace();
    }
    return ResponseEntity.ok(new MessageResponse(msg));

//      service.createTask(taskEntity);
//      return ResponseEntity.ok(new MessageResponse("Task created successfully!"));
//    }
  }
  @PostMapping("/update")
  public ResponseEntity<?> taskUpdate(@RequestBody @Valid TaskRequest taskRequest) throws ParseException {
    TaskEntity taskEntity = null;
    taskEntity = new TaskEntity();
    taskEntity.setId(taskRequest.getId());
    taskEntity.setDays(taskRequest.getDays());
    taskEntity.setUsername(taskRequest.getUsername());
    taskEntity.setLogInDate(taskRequest.getLogInDate());
    taskEntity.setLogOutDate(taskRequest.getLogOutDate());
    taskEntity.setLogInTime(taskRequest.getLogInTime());
    taskEntity.setLogOutTime(taskRequest.getLogOutTime());

    Date date1 = new SimpleDateFormat("HH:mm").parse(taskRequest.getLogInTime());
    Date date2 = new SimpleDateFormat("HH:mm").parse(taskRequest.getLogOutTime());
    long diff = date2.getTime() - date1.getTime();
    System.out.println(formatDuration(diff));
    Date spend = new SimpleDateFormat("HH:mm").parse(formatDuration(diff));
    Date total = new SimpleDateFormat("HH:mm").parse("09:00");
    long calHrs = spend.getTime() - total.getTime();

    System.out.println(formatDuration(calHrs));
    if(formatDuration(calHrs).contains("-")){
      taskEntity.setLessHrs(formatDuration(calHrs).replace("-",""));
      taskEntity.setExtraHrs(("00:00"));
    }
    else {
      taskEntity.setExtraHrs(formatDuration(calHrs));
      taskEntity.setLessHrs(("00:00"));
    }
    taskEntity.setSpendHrs(formatDuration(diff));
    taskEntity.setProjectName(taskRequest.getProjectName());
    taskEntity.setModule(taskRequest.getModule());
    taskEntity.setRemarks(taskRequest.getRemarks());
    taskEntity.setMonth(taskRequest.getMonth());
    taskEntity.setYear(taskRequest.getYear());
    service.createTask(taskEntity);
    return ResponseEntity.ok(new MessageResponse("Task updated successfully!"));
  }
  String getMonthForInt(int num) {
    String month = "wrong";
    DateFormatSymbols dfs = new DateFormatSymbols();
    String[] months = dfs.getMonths();
    if (num >= 0 && num <= 11 ) {
      month = months[num];
    }
    return month;
  }
  @PostMapping("/getTask")
  public ResponseEntity<TaskEntity> getTask(@RequestBody @Valid TaskRequest taskRequest) throws ParseException {
    List<TaskEntity> task=null;
    LocalDate today = LocalDate.now();
    DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
    String log_in = dateFormat.format(new Date());
    LocalDate dt = LocalDate.parse(log_in);
    int year = today.getYear();
    if(taskRequest.getMonth()!=null && !taskRequest.getMonth().isEmpty() && taskRequest.getYear()>0){
      task = service.getTask(taskRequest.getUsername(), taskRequest.getMonth(), taskRequest.getYear());
    }
    else {
      task= service.getTask(taskRequest.getUsername(), dt.getMonth().name(), year);
    }
    if(task.size()>0) {
      return new ResponseEntity(task, HttpStatus.ACCEPTED);
    }
    else {

      return new ResponseEntity("No Record Found", HttpStatus.ACCEPTED);
    }
  }
  @PostMapping("/getHrs")
  public ResponseEntity<TaskEntity> getLess(@RequestBody @Valid TaskRequest taskRequest) throws ParseException {
    HashMap<String,String> hm = new HashMap<String,String>();
    LocalDate today = LocalDate.now();
    DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
    String log_in = dateFormat.format(new Date());
    String finalDay = new SimpleDateFormat("EEEE").format(dateFormat.parse(log_in));
    LocalDate dt = LocalDate.parse(log_in);
    int year = today.getYear();
    String extrahrs = service.getExtrahrs(taskRequest.getUsername(), dt.getMonth().name(), year,finalDay);
    String lessHrs = service.getLessHrs(taskRequest.getUsername(), dt.getMonth().name(), year,finalDay);

    System.out.println(extrahrs+ " : "+lessHrs);
    Date date1 = new SimpleDateFormat("HH:mm").parse(extrahrs);
    Date date2 = new SimpleDateFormat("HH:mm").parse(lessHrs);

    long diff = date1.getTime() - date2.getTime();
    System.out.println(formatDuration(diff));
    if(formatDuration(diff).contains("-")){
      hm.put("less",formatDuration(diff));
    }else {
      hm.put("extra",formatDuration(diff));
    }

    if(hm!=null) {
      return new ResponseEntity(hm, HttpStatus.ACCEPTED);
    }
    else {
      return new ResponseEntity("No Record Find", HttpStatus.ACCEPTED);
    }
  }

  @PostMapping("/applyLeave")
  public ResponseEntity<?> applyLeave(@RequestBody @Valid LeaveRequest leaveRequest) throws ParseException {
    if (userRepository.existsByUsername(leaveRequest.getUsername())) {
      User user = userRepository.findUserByUsername(leaveRequest.getUsername());
      Email.sendMail(leaveRequest.getUsername(),leaveRequest.getFrom(),user.getManagerEmail(),leaveRequest.getCc(),leaveRequest.getSubject(),leaveRequest.getBody(),user.getManager());
    }
    return ResponseEntity.ok(new MessageResponse("Leave Applied Successfully!"));
  }

  @PostMapping("/holidays")
  public ResponseEntity<HolidayEntity> holidays(@RequestBody @Valid HolidayRequest holidayRequest) throws ParseException {
    LocalDate today = LocalDate.now();
    int year = today.getYear();
    List<HolidayEntity> task = service.getHolidays(year);
    if(task.size()>0) {
      return new ResponseEntity(task, HttpStatus.ACCEPTED);
    }
    else {
      return new ResponseEntity("No Record Find", HttpStatus.ACCEPTED);
    }
  }
  private static String formatDuration(long duration) {
    long hours = TimeUnit.MILLISECONDS.toHours(duration);
    long minutes = TimeUnit.MILLISECONDS.toMinutes(duration) % 60;
    long seconds = TimeUnit.MILLISECONDS.toSeconds(duration) % 60;
    long milliseconds = duration % 1000;
    return String.format("%02d:%02d", hours, minutes);
  }

  @GetMapping("/birthday")
  public ResponseEntity<User> birthday() {
    DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
    String todayDate = dateFormat.format(new Date());
    List<User> collect = userRepository.findAll().stream().filter(noti -> noti.getDob()!=null && !noti.getDob().isEmpty() && noti.getDob().equalsIgnoreCase(todayDate))
        .collect(Collectors.toList());
    return new ResponseEntity(collect, HttpStatus.ACCEPTED);
  }
  @PostMapping("/work_log")
  public ResponseEntity<?> workLog(@RequestBody @Valid WorkLogRequest workLogRequest) throws ParseException {
    LocalDate dt = LocalDate.parse(workLogRequest.getDateStarted());
    DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
    String todayDate = dateFormat.format(new Date());
    WorkLog workLog = new WorkLog();
    WorkLog workLog1 = service.workLog(workLogRequest.getUsername(), workLogRequest.getDateStarted());
    if(workLog1==null){
      workLog.setUsername(workLogRequest.getUsername());
      workLog.setDateStarted(workLogRequest.getDateStarted());
      workLog.setProject(workLogRequest.getProject());
      workLog.setJiraTask(workLogRequest.getJiraTask());
      workLog.setJiraTicketNo(workLogRequest.getJiraTicketNo());
      workLog.setTimeSpend(workLogRequest.getTimeSpend());
      workLog.setWorkDescription(workLogRequest.getWorkDescription());
      workLog.setMonth(dt.getMonth().name());
      service.workLog(workLog);
    }else {

      workLog.setId(workLog1.getId());
      workLog.setUsername(workLog1.getUsername());
      workLog.setDateStarted(workLogRequest.getDateStarted());
      workLog.setProject(workLogRequest.getProject());
      workLog.setJiraTask(workLogRequest.getJiraTask());
      workLog.setJiraTicketNo(workLogRequest.getJiraTicketNo());
      workLog.setTimeSpend(workLogRequest.getTimeSpend());
      workLog.setWorkDescription(workLogRequest.getWorkDescription());
      workLog.setMonth(workLog1.getMonth());
      service.workLog(workLog);
    }
    return ResponseEntity.ok(new MessageResponse("work log created Successfully!"));
  }
  @PostMapping("/getWorkLog")
  public ResponseEntity<WorkLog> getWork(@RequestBody @Valid WorkLogRequest workLogRequest) throws ParseException {
    List<WorkLog> workLogs=null;
    DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
    String log_in = dateFormat.format(new Date());
    LocalDate dt = LocalDate.parse(log_in);
    if(workLogRequest.getMonth()!=null && !workLogRequest.getMonth().isEmpty()){
      workLogs = service.getWorkLogs(workLogRequest.getUsername(), workLogRequest.getMonth());
    }
    else {
      workLogs= service.getWorkLogs(workLogRequest.getUsername(), dt.getMonth().name());
    }
    if(workLogs.size()>0) {
      return new ResponseEntity(workLogs, HttpStatus.ACCEPTED);
    }
    else {

      return new ResponseEntity("No Record Found", HttpStatus.ACCEPTED);
    }
  }

  @PostMapping("/getWorkLogMonth")
  public ResponseEntity<MessageResponse> getWorkMonth(@RequestBody @Valid WorkLogRequest workLogRequest) throws ParseException {
    List<WorkLog> workLogs=null;
    User userByUsername = userRepository.findUserByUsername(workLogRequest.getUsername());
    DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
    String log_in = dateFormat.format(new Date());
    LocalDate dt = LocalDate.parse(log_in);
    if(workLogRequest.getMonth()!=null && !workLogRequest.getMonth().isEmpty() && userByUsername!=null){
      workLogs = service.getWorkLogs(workLogRequest.getUsername(), workLogRequest.getMonth());
    }
    if(workLogs.size()>0 && userByUsername!=null) {
      Email.sendLogMail(workLogRequest.getUsername(),userByUsername.getEmail(),userByUsername.getManagerEmail(),"Work Log",workLogs,null,null,userByUsername.getManager(),workLogRequest.getMonth(),userByUsername.getFirstname().concat(" ").concat(userByUsername.getLastname()));
      return new ResponseEntity(workLogs, HttpStatus.ACCEPTED);
    }
    else {
      return ResponseEntity
          .badRequest()
          .body(new MessageResponse("No Record Found!"));
    }
  }
  @PostMapping("/getBillingMonth")
  public ResponseEntity<MessageResponse> getBillingMonth(@RequestBody @Valid WorkLogRequest workLogRequest) throws ParseException {
    List<TaskEntity> workLogs=null;
    User userByUsername = userRepository.findUserByUsername(workLogRequest.getUsername());
    LocalDate today = LocalDate.now();
    int year = today.getYear();
    int srNo =1;
    List<BillingEntity> billingEntityList;
    BillingEntity  response = null;
    billingEntityList = new ArrayList<>();
    if(workLogRequest.getMonth()!=null && !workLogRequest.getMonth().isEmpty() && userByUsername!=null){
      workLogs = service.getTask(workLogRequest.getUsername(), workLogRequest.getMonth(),year);
    }
    if(workLogs.size()>0 && userByUsername!=null) {

        Email.sendBillingMail(workLogRequest.getUsername(), userByUsername.getEmail(),
            userByUsername.getManagerEmail(), "Consultant Billing", workLogs,
            null, null, userByUsername.getManager(), workLogRequest.getMonth(), userByUsername.getFirstname().concat(" ").concat(userByUsername.getLastname()), year);

        for(TaskEntity taskEntity :workLogs){
          response = new BillingEntity();
          response.setSr_NO(srNo++);
          response.setProject_Name(taskEntity.getProjectName());
          response.setResource_Name(userByUsername.getFirstname().concat(" ").concat(userByUsername.getLastname()));
          response.setDate(taskEntity.getLogInDate());
          response.setStart_Time(taskEntity.getLogInTime());
          response.setEnd_Time(taskEntity.getLogOutTime());
          response.setJIRA_No(taskEntity.getModule());
          billingEntityList.add(response);
        }

      return new ResponseEntity(billingEntityList, HttpStatus.ACCEPTED);
    }
    else {
      return ResponseEntity
          .badRequest()
          .body(new MessageResponse("No Record Found!"));
    }
  }
  @PostMapping("/getBillingDate")
  public ResponseEntity<MessageResponse> getBillingDate(@RequestBody @Valid WorkLogRequest workLogRequest) throws ParseException {
    User userByUsername = userRepository.findUserByUsername(workLogRequest.getUsername());
    List<TaskEntity> workLogs=null;
    List<BillingEntity> billingEntityList;
    LocalDate today = LocalDate.now();
    int year = today.getYear();
    int srNo = 1;
    BillingEntity  response = null;
    billingEntityList = new ArrayList<>();
    if(workLogRequest.getDateStarted()!=null && !workLogRequest.getDateStarted().isEmpty() && workLogRequest.getDateEnded()!=null && !workLogRequest.getDateEnded().isEmpty()){
      workLogs = service.getBillingLogDate(workLogRequest.getUsername(), workLogRequest.getDateStarted(),workLogRequest.getDateEnded(),year);
    }
    if(workLogs.size()>0 && userByUsername!=null) {

        Email.sendBillingMail(workLogRequest.getUsername(), userByUsername.getEmail(), userByUsername.getManagerEmail(), "Consultant Billing", workLogs, workLogRequest.getDateStarted(), workLogRequest.getDateEnded(), userByUsername.getManager(), null, userByUsername.getFirstname().concat(" ").concat(userByUsername.getLastname()), year);

        for (TaskEntity taskEntity : workLogs) {
          response = new BillingEntity();
          response.setSr_NO(srNo++);
          response.setProject_Name(taskEntity.getProjectName());
          response.setResource_Name(userByUsername.getFirstname().concat(" ").concat(userByUsername.getLastname()));
          response.setDate(taskEntity.getLogInDate());
          response.setStart_Time(taskEntity.getLogInTime());
          response.setEnd_Time(taskEntity.getLogOutTime());
          response.setJIRA_No(taskEntity.getModule());
          billingEntityList.add(response);
        }

      return new ResponseEntity(billingEntityList, HttpStatus.ACCEPTED);
    }
    else {
      return
          ResponseEntity
              .badRequest()
              .body(new MessageResponse("No Record Found!"));
    }
  }
}
