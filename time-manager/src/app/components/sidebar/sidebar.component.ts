import { Component, OnInit } from '@angular/core';
import { TokenStorageService } from 'src/app/services/token-storage.service';
import { TaskService } from 'src/app/services/task.service';
import { AuthService } from 'src/app/services/auth.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-sidebar',
  templateUrl: './sidebar.component.html',
  styleUrls: ['./sidebar.component.scss']
})
export class SidebarComponent implements OnInit {
  private roles: string[];
  isLoggedIn = false;
  showAdminBoard = false;
  showModeratorBoard = false;
  username: string;
  email: string;
  role: string;
  extra: string;
  logInDate: string
  less: string;
  task: any[];
  form: any = {};
  isExpanded = true;
  showSubmenu: boolean = false;
  isShowing = false;
  showSubSubMenu: boolean = false;
  isSuccessful = false;
  isSignUpFailed = false;
  errorMessage = '';
  msg : string;
  company:string;
  constructor(private tokenStorageService:TokenStorageService,private taskService:TaskService, private authService:AuthService , private router: Router) { 
    this.isLoggedIn = !!this.tokenStorageService.getToken();
    if (this.isLoggedIn) {
      const user = this.tokenStorageService.getUser();
      this.username = user.username;
      this.getUser();
    }
  }

  ngOnInit() { }
  getTask() {
    this.taskService.getTask(this.form).subscribe(
      data => {
        this.task=data;
        this.isSuccessful =true;
        this.isSignUpFailed = false;
      },
      err => {
        this.errorMessage = err.error.message;
        this.isSignUpFailed = true;
      }
    );
  }
  getUser() {
    this.authService.getProfile().subscribe(
      data => {
        this.email = data.email;
        this.company = data.company;
        this.role = data.roles;
        if(data.email==null || data.email == undefined || data.email===""){
          this.router.navigate(['/update-profile']);
        }
      }
    );
  }
}
