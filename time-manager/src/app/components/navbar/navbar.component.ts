import { AppService } from './../../services/app.service';
import { Component, OnInit } from '@angular/core';
import { TaskService } from 'src/app/services/task.service';
import { TokenStorageService } from 'src/app/services/token-storage.service';
import { AuthService } from 'src/app/services/auth.service';

@Component({
  selector: 'app-navbar',
  templateUrl: './navbar.component.html',
  styleUrls: ['./navbar.component.scss']
})
export class NavbarComponent implements OnInit {
  less: string;
  extra: string;
  loss = false;
  gain = false;
  notifyCount: number;
  record: any[];
  username: string;
  isLoggedIn = false;
  data: Object;

  constructor(private appService: AppService, private taskService: TaskService, private tokenStorageService: TokenStorageService, private authService: AuthService) {
    this.isLoggedIn = !!this.tokenStorageService.getToken();
    if (this.isLoggedIn) {
      this.getUser();
      this.getHrs();
      this.getBirthday();
    }
  }
  isCollapsed = true;
  ngOnInit() { }

  toggleSidebarPin() {
    this.appService.toggleSidebarPin();
  }
  toggleSidebar() {
    this.appService.toggleSidebar();
  }
  getHrs() {
    this.taskService.getCalHours().subscribe(
      response => {
        if (response.less != null) {
          this.less = response.less;
          this.loss = true;
        } else {
          this.extra = response.extra;
          this.gain = true;
        }
      }
    );
  }
  getBirthday() {
    this.taskService.getBirthday().subscribe(
      response => {
        this.record=response;
        console.log(this.record);
        this.notifyCount = response.length;
      }
    );
  }
  getUser() {
    this.authService.getProfile().subscribe(
      data => {
        this.username = data.firstname;
      }
    );
  }
  logout() {
    this.tokenStorageService.signOut();
    window.location.reload();
  }
}
