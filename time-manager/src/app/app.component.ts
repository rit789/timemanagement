import { Component, HostListener } from '@angular/core';
import { AppService } from './services/app.service';
import { TokenStorageService } from './services/token-storage.service';
import { AuthService } from './services/auth.service';
import { Router } from '@angular/router';
import { TaskService } from './services/task.service';
@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  title = 'pro-dashboard-angular';
  task: any[];
  form: any = {};
  isSuccessful = false;
  isSignUpFailed = false;
  errorMessage = '';
  data:any;
  msg:string;
  postList: any[] = [];
  active:boolean;
  isLoggedIn = false;
  isLoginFailed = false;
  currentURL='';
  isExits=true;
  constructor(private appService: AppService,private tokeService:TokenStorageService,private authService: AuthService,
    private router: Router,private taskService:TaskService) {
   
    if (window.location.hash.match("#/user-registration")) {
      this.isExits=false;
     
    }
    if (window.location.hash.match("#/change-password")) {
      this.isExits=false;
    }
    if (this.tokeService.getToken()) {
      this.isLoggedIn = true;
    }

  }
  ngOnInit() { }
  getClasses() {
    const classes = {
      'pinned-sidebar': this.appService.getSidebarStat().isSidebarPinned,
      'toggeled-sidebar': this.appService.getSidebarStat().isSidebarToggeled
    }
    return classes;
  }
  toggleSidebar() {
    this.appService.toggleSidebar();
  }
  onSubmit() {
    this.authService.login(this.form).subscribe(
      data => {
        this.tokeService.saveToken(data.accessToken);
        this.tokeService.saveUser(data);
        this.isLoginFailed = false;
        this.isLoggedIn = true;
         if (this.isLoggedIn) {
          this.autoPunch() ;
          this.router.navigate(['/dashboard']);
          this.reloadPage();
        }
        else{
          this.isLoginFailed = true;
          this.errorMessage =data.message;
        }
      },
      err => {
        this.errorMessage = err.error.message;
        this.isLoginFailed = true;
      }
    );
  }
  reloadPage() {
    window.location.reload();
  }
  autoPunch() {
    this.taskService.register(this.form).subscribe();
  }
}
