import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';
import * as FileSaver from 'file-saver';
import * as XLSX from 'xlsx';
import { environment } from 'src/environments/environment.prod';
import { TokenStorageService } from './token-storage.service';

const AUTH_API = environment.TASK_API;
const EXCEL_TYPE = 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet;charset=UTF-8';
const EXCEL_EXTENSION = '.xlsx';
const httpOptions = {
  headers: new HttpHeaders({ 'Content-Type': 'application/json' })
};
@Injectable({
  providedIn: 'root'
})
export class TaskService {
  [x: string]: any;
  username:string;

  constructor(private http: HttpClient,private token:TokenStorageService) {
    if(token.getUser()!=null){
      this.username = token.getUser().username;
    }
  }
  
  register(user): Observable<any> {
    const username = JSON.parse(sessionStorage.getItem('auth-user')).username;
    return this.http.post(AUTH_API + 'create', {
      username,
      projectName: "test",
      module: "test",
      remarks: "test"
    }, httpOptions);
  }
  
  manualPunch(user): Observable<any> {
    this.username = this.token.getUser().username
    return this.http.post(AUTH_API + 'manual', {
      username:this.username,
      logInDate:user.loginDate,
      logOutDate:user.logOutDate,
      logInTime:user.logInTime,
      logOutTime:user.logOutTime,
      projectName: user.projectName,
      module: user.module,
      remarks: user.remarks
    }, httpOptions);
  }

  getTask(user): Observable<any> {
    const username = JSON.parse(sessionStorage.getItem('auth-user')).username;
    return this.http.post(AUTH_API + 'getTask', {
      username:username,
      month:user.month,
      year:user.year  
    }, httpOptions);
  }
  getHoliday(): Observable<any> {
    return this.http.post(AUTH_API + 'holidays', {
    }, httpOptions);
  }
 
  getDetails(): Observable<any> {
    
    return this.http.post(AUTH_API + 'getTask', {
      username:this.username,
    }, httpOptions);
  }
  getCalHours(): Observable<any> {
    const username = JSON.parse(sessionStorage.getItem('auth-user')).username;
    return this.http.post(AUTH_API + 'getHrs', {
      username:username,
    }, httpOptions);
  }
  updatePost(user): Observable<any> {
    return this.http.post(AUTH_API + 'update', {
      id: user.id,
      username: user.username,
      days: user.days,
      logInDate: user.logInDate,
      logOutDate: user.logOutDate,
      logInTime: user.logInTime,
      logOutTime: user.logOutTime,
      spendHrs: user.spendHrs,
      projectName: user.projectName,
      remarks: user.remarks,
      module: user.module,
      month: user.month,
      year: user.year
    }, httpOptions);
  }
  workLog(record): Observable<any> {
    return this.http.post(AUTH_API + 'work_log', {
      username:this.username,
      id:record.id,
      dateStarted:record.dateStarted,
      project:record.project,
      jiraTask:record.jiraTask,
      jiraTicketNo:record.jiraTicketNo,
      timeSpend:record.timeSpend,
      workDescription:record.workDescription
    }, httpOptions);
  }
  getWorkLog(work): Observable<any> {
    const username = JSON.parse(sessionStorage.getItem('auth-user')).username;
    return this.http.post(AUTH_API + 'getWorkLog', {
      username:username,
      month:work.month,
    }, httpOptions);
  }
  getBillingLogMonth(work): Observable<any> {
    const username = JSON.parse(sessionStorage.getItem('auth-user')).username;
    return this.http.post(AUTH_API + 'getBillingMonth', {
      username:username,
      month:work.month,
    }, httpOptions);
  }
  getBilingDate(work): Observable<any> {
    const username = JSON.parse(sessionStorage.getItem('auth-user')).username;
    return this.http.post(AUTH_API + 'getBillingDate', {
      username:username,
      dateStarted:work.dateStarted,
      dateEnded:work.dateEnded,
    }, httpOptions);
  }

  getWorkLogMonth(work): Observable<any> {
    const username = JSON.parse(sessionStorage.getItem('auth-user')).username;
    return this.http.post(AUTH_API + 'getWorkLogMonth', {
      username:username,
      month:work.month,
    }, httpOptions);
  }
  getWorkLogDate(work): Observable<any> {
    const username = JSON.parse(sessionStorage.getItem('auth-user')).username;
    return this.http.post(AUTH_API + 'getWorkLogDate', {
      username:username,
      dateStarted:work.dateStarted,
      dateEnded:work.dateEnded,
    }, httpOptions);
  }
  applyLeave(user): Observable<any> {
    const email = JSON.parse(sessionStorage.getItem('auth-user')).email;
    const   manager = JSON.parse(sessionStorage.getItem('auth-user')).manager;
    const to = JSON.parse(sessionStorage.getItem('auth-user')).managerEmail;
    return this.http.post(AUTH_API + 'applyLeave', {
      username:this.username,
      from:email,
      managerEmail:to,
      cc:user.cc,
      subject:user.subject,
      body:user.body,
      manager:manager
    }, httpOptions);
  }
  toggleSidebar() {
    this.isSidebarToggeled = ! this.isSidebarToggeled;
  }

  toggleSidebarPin() {
    this.isSidebarPinned = ! this.isSidebarPinned;
  }

  getSidebarStat() {
    return {
      isSidebarPinned: this.isSidebarPinned,
      isSidebarToggeled: this.isSidebarToggeled
    }
  }
  public exportAsExcelFile(json: any[], excelFileName: string): void {
    const worksheet: XLSX.WorkSheet = XLSX.utils.json_to_sheet(json)
    const workbook: XLSX.WorkBook = { Sheets: { 'data': worksheet }, SheetNames: ['data'] };
    const excelBuffer: any = XLSX.write(workbook, { bookType: 'xlsx', type: 'array' });
    this.saveAsExcelFile(excelBuffer, excelFileName);
  }
  private saveAsExcelFile(buffer: any, fileName: string): void {
    const data: Blob = new Blob([buffer], {
      type: EXCEL_TYPE
    });
    FileSaver.saveAs(data, fileName + '- report' + new Date().getTime() + EXCEL_EXTENSION);
  }
  getBirthday(): Observable<any> {
    return this.http.get(AUTH_API + 'birthday', {
    } );
  }
}
