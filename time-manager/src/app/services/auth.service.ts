import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';
import { environment } from 'src/environments/environment.prod';

const AUTH_API = environment.AUTH_API;

const httpOptions = {
  headers: new HttpHeaders({ 'Content-Type': 'application/json' })
};

@Injectable({
  providedIn: 'root'
})
export class AuthService {
  constructor(private http: HttpClient) { }
  login(credentials): Observable<any> {
    return this.http.post(AUTH_API + 'signin', {
      username: credentials.username,
      password: credentials.password
    }, httpOptions);
  }

  register(user): Observable<any> {
    console.log(user.username);
    console.log(user.email);
    return this.http.post(AUTH_API + 'signup', {
      username:user.username,
      firstname:user.firstname,
      lastname:user.lastname,
      password:user.password,
      email:user.email,
      company:user.company,
      designation:user.designation,
      manager:user.manager,
      managerEmail:user.managerEmail,
      role:user.role
    }, httpOptions);
  }
  updateUser(user): Observable<any> {
    console.log(user.dob);
    const username = JSON.parse(sessionStorage.getItem('auth-user')).username;
    return this.http.post(AUTH_API + 'update-profile', {
      username,
      firstname:user.firstname,
      lastname:user.lastname,
      dob:user.dob,
      email:user.email,
      company:user.company,
      designation:user.designation,
      manager:user.manager,
      managerEmail:user.managerEmail
    }, httpOptions);
  }
  changePassword(user): Observable<any> {
    return this.http.post(AUTH_API + 'changePassword', {
      email: user.email,
      otp: user.otp,
      password: user.password
    }, httpOptions);
  }
  getOtp(email: string): Observable<any> {
    return this.http.post(AUTH_API + 'getOtp', {
      email: email,
    }, httpOptions);
  }
  getProfile(): Observable<any> {
    const username = JSON.parse(sessionStorage.getItem('auth-user')).username;
    return this.http.post(AUTH_API + 'profile', {
      username
    }, httpOptions);
  }
}