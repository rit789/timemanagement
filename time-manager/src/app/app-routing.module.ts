import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { DashboardComponent } from './pages/dashboard/dashboard.component';
import { LeaveComponent } from './pages/leave/leave.component';
import { FormsComponent } from './pages/manual-punch/forms.component';
import { ProfileComponent } from './pages/profile/profile.component';
import { UserRegistrationComponent } from './pages/user-registration/user-registration.component';
import { LeaveCalenderComponent } from './pages/leave-calender/leave-calender.component';
import { ChangePasswordComponent } from './pages/change-password/change-password.component';
import { AppComponent } from './app.component';
import { WorkLogComponent } from './pages/work-log/work-log.component';
import { WorkLogRaiseRequestComponent } from './pages/work-log-raise-request/work-log-raise-request.component';
import { BillingLogsComponent } from './pages/billing-logs/billing-logs.component';


const routes: Routes = [
  {path: 'dashboard', component: DashboardComponent},
  {path: 'user-registration', component: UserRegistrationComponent},
  {path: 'punch', component: FormsComponent},
  {path: 'leave', component: LeaveComponent},
  {path: 'holidays', component: LeaveCalenderComponent},
  {path: 'update-profile', component: ProfileComponent},
  {path: 'change-password', component: ChangePasswordComponent},
  {path: 'work-log',component:WorkLogComponent},
  {path: 'work-log-raise-request',component:WorkLogRaiseRequestComponent},
  {path: 'billing_logs',component:BillingLogsComponent},
  {path: '**', component: DashboardComponent},
  {path: '**', pathMatch: 'full', redirectTo: 'dashboard' }
];

@NgModule({
  imports: [RouterModule.forRoot(routes , { useHash: true })],
  exports: [RouterModule]
})
export class AppRoutingModule { }
