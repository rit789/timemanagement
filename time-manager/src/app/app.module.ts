import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { CollapseModule } from 'ngx-bootstrap/collapse';
import { ToastrModule } from 'ngx-toastr';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { FormsModule } from '@angular/forms';
import { NavbarComponent } from './components/navbar/navbar.component';
import { SidebarComponent } from './components/sidebar/sidebar.component';
import { FooterComponent } from './components/footer/footer.component';
import { DashboardComponent } from './pages/dashboard/dashboard.component';
import { LeaveComponent } from './pages/leave/leave.component';
import { FormsComponent } from './pages/manual-punch/forms.component';
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';
import { ProfileComponent } from './pages/profile/profile.component';
import { UserRegistrationComponent } from './pages/user-registration/user-registration.component';
import { LeaveCalenderComponent } from './pages/leave-calender/leave-calender.component';
import { ChangePasswordComponent } from './pages/change-password/change-password.component';
import { ErrorInterceptor } from 'src/helper/error.interceptor';
import { JwtInterceptor } from 'src/helper/jwt.interceptor';
import { WorkLogComponent } from './pages/work-log/work-log.component';
import { WorkLogRaiseRequestComponent } from './pages/work-log-raise-request/work-log-raise-request.component';
import { BillingLogsComponent } from './pages/billing-logs/billing-logs.component';
@NgModule({
  declarations: [
    AppComponent,
    NavbarComponent,
    SidebarComponent,
    FooterComponent,
    DashboardComponent,
    LeaveComponent,
    FormsComponent,
    ProfileComponent,
    UserRegistrationComponent,
    LeaveCalenderComponent,
    ChangePasswordComponent,
    WorkLogComponent,
    WorkLogRaiseRequestComponent,
    BillingLogsComponent,

  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    CommonModule,
    BrowserAnimationsModule,
    HttpClientModule,
    BrowserModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    BrowserModule,
    AppRoutingModule,
    FormsModule,
    HttpClientModule,  
    CollapseModule.forRoot(),
    ToastrModule.forRoot()
    ],
  providers: [  { provide: HTTP_INTERCEPTORS, useClass: JwtInterceptor, multi: true },
    { provide: HTTP_INTERCEPTORS, useClass: ErrorInterceptor, multi: true }],
  bootstrap: [AppComponent]
})
export class AppModule { }
