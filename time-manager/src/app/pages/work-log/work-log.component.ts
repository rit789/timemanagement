import { Component, OnInit } from '@angular/core';
import { TokenStorageService } from 'src/app/services/token-storage.service';
import { TaskService } from 'src/app/services/task.service';
import { Router } from '@angular/router';
@Component({
  selector: 'app-work-log',
  templateUrl: './work-log.component.html',
  styleUrls: ['./work-log.component.scss']
})
export class WorkLogComponent implements OnInit {

  worklogs: any[];
  form: any = {};
  isSuccessful = false;
  isSignUpFailed = false;
  errorMessage = '';
  editField: string;
  bsModalRef: any;
  data: any;
  msg: string;
  postList: any[] = [];
  active: boolean;
  isLoggedIn = false;
  isLoginFailed = false;
  username: string;
  roles: string[] = [];
  returnUrl: string;
  excelData: string[] = [];
  // year: any = ['2020', '2021', '2022', '2023', '2024', '2025', '2026', '2027', '2028', '2029', '2030'];
  month: any =
    ['JANUARY', 'FEBRUARY', 'MARCH', 'APRIL', 'MAY', 'JUNE', 'JULY', 'AUGUST', 'SEPTEMBER', 'OCTOBER', 'NOVEMBER', 'DECEMBER'];
  constructor(private taskService: TaskService, private tokeService: TokenStorageService, private router: Router) {
      if (this.tokeService.getToken()) {
        this.isLoggedIn = true;
        this.roles = this.tokeService.getUser().roles;
        this.username = this.tokeService.getUser().username;
      }
  }
  ngOnInit() {

  }
  onCreated() {
    this.taskService.workLog(this.form).subscribe(
      data => {
        this.msg = data.message
        this.isSuccessful = true;
        this.isSignUpFailed = false;
        this.router.navigate(['/dashboard']);
      },
      err => {
        this.errorMessage = err.error.message;
        this.isSignUpFailed = true;
      }
    );
  }
  
  getWorkLog() {
    this.taskService.getWorkLog(this.form).subscribe(
      data => {
        if (data != null) {
          this.worklogs = data;
          this.excelData = data;
          this.isSuccessful = true;
          this.isSignUpFailed = false;
        }
        else {
          this.isSignUpFailed = true;
          this.msg = "No Record Found";
        }
      },
      err => {
        this.errorMessage = err.error.message;
        this.isSignUpFailed = true;
      }
    );
  }

  updateList(employee: any) {
    console.log(employee);
    this.taskService.updatePost(employee).subscribe(
      data => {
        this.isSuccessful = true;
        this.msg = data.message;
        this.isSignUpFailed = false;
        this.reloadPage();

      },
      err => {
        this.errorMessage = err.error.message;
        this.isSignUpFailed = true;
      }
    );
  }

  reloadPage() {
    window.location.reload();
  }
  
  exportAsXLSX(): void {
    this.getWorkLog();
    this.taskService.exportAsExcelFile(this.excelData, this.username);
  }
}

