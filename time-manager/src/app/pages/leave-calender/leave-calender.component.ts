import { Component, OnInit } from '@angular/core';
import { TaskService } from 'src/app/services/task.service';
import { TokenStorageService } from 'src/app/services/token-storage.service';
import { DatePipe } from '@angular/common';

@Component({
  selector: 'app-leave-calender',
  templateUrl: './leave-calender.component.html',
  styleUrls: ['./leave-calender.component.scss'],
  providers: [DatePipe]
})
export class LeaveCalenderComponent implements OnInit {
  data: any;
  msg: string;
  excelData: any[] = [];
  year: number;
  isLoggedIn = false;
  myDate = new Date();
  today:string
  holiday:boolean;
  constructor(private taskService: TaskService, private tokenStorageService: TokenStorageService,private datePipe: DatePipe) {
    this.today = this.datePipe.transform(this.myDate, 'yyyy-MM-dd');
    
   }

  ngOnInit() {
    this.isLoggedIn = !!this.tokenStorageService.getToken();
    if (this.isLoggedIn) {
      const user = this.tokenStorageService.getUser();
      this.getHolidays();
    }

  }
  getHolidays() {
    console.log(this.today);
    this.taskService.getHoliday().subscribe(
      data => {
        this.data = data;
        this.excelData = data;
        this.year = data.year;
      
        for(let holidays of this.data) {
          if(holidays.date===this.today){
            this.holiday=true;
          }
        }
      },
    );
  }
  exportAsXLSX(): void {
    this.getHolidays();
    this.taskService.exportAsExcelFile(this.excelData, 'holidays');
  }
}
