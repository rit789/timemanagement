import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { LeaveCalenderComponent } from './leave-calender.component';

describe('LeaveCalenderComponent', () => {
  let component: LeaveCalenderComponent;
  let fixture: ComponentFixture<LeaveCalenderComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ LeaveCalenderComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(LeaveCalenderComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
