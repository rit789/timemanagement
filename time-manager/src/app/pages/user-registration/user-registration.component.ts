import { Component, OnInit } from '@angular/core';
import { TaskService } from '../../services/task.service';
import { AuthService } from '../../services/auth.service';
import { TokenStorageService } from '../../services/token-storage.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-user-registration',
  templateUrl: './user-registration.component.html',
  styleUrls: ['./user-registration.component.scss']
})
export class UserRegistrationComponent implements OnInit {

  task: any[];
  form: any = {};
  isSuccessful = false;
  isSignUpFailed = false;
  errorMessage = '';
  editField: string;
  bsModalRef: any;
  postList: any[] = [];
  msg:string
  isLoggedIn = false;
  isLoginFailed = false;
  constructor(private taskService: TaskService,private authService:AuthService,private tokenService:TokenStorageService,private router:Router) {
    
   }
  ngOnInit() {
    
    if (this.tokenService.getToken()) {
      this.isLoggedIn = true;
     
    }
  }
  user_registration() {
    this.authService.register(this.form).subscribe(
      data => {
        console.log(data);
        this.isSuccessful = true;
        this.msg=data.message
        this.isSignUpFailed = false;
        this.router.navigate(['/dashboard']);
        this.reloadPage();
      },
      err => {
        this.errorMessage = err.error.message;
        this.isSignUpFailed = true;
        this.msg=err.error.message;
      }
    );
  }
  reloadPage() {
    window.location.reload();
  }
}