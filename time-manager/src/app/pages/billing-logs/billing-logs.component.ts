import { Component, OnInit } from '@angular/core';
import { TaskService } from '../../services/task.service';
import { TokenStorageService } from '../../services/token-storage.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-billing-logs',
  templateUrl: './billing-logs.component.html',
  styleUrls: ['./billing-logs.component.scss']
})
export class BillingLogsComponent implements OnInit {

  recordDatePeriod: any[];
  worklogsMonth: any[];
  form: any = {};
  workLogData: string[] = [];
  isSuccessful:boolean = false;
  isSignUpFailed = false;
  errorMessage = '';
  editField: string;
  bsModalRef: any;
  msg: string;
  isLoggedIn = false;
  isLoginFailed = false;
  username: string;
  month: any =
    ['JANUARY', 'FEBRUARY', 'MARCH', 'APRIL', 'MAY', 'JUNE', 'JULY', 'AUGUST', 'SEPTEMBER', 'OCTOBER', 'NOVEMBER', 'DECEMBER'];
  constructor(private taskService: TaskService, private tokeService: TokenStorageService, private router: Router) {
      if (this.tokeService.getToken()) {
        this.isLoggedIn = true;
        this.username = this.tokeService.getUser().username;
      }
  }
  ngOnInit() {

  }

  getBillMonth() {
      this.taskService.getBillingLogMonth(this.form).subscribe(
        response => {
          if (response!==null) {
            this.worklogsMonth = response;
            this.isSuccessful = true;
            this.isSignUpFailed = false;
            this.msg="Request Sended Successfully";
            this.reloadPage();
          }
        },
        err => {
          this.errorMessage = "No Record Found.";
          this.isSignUpFailed = true;
        }
      );
    }
    getBillDate() {
      this.taskService.getBilingDate(this.form).subscribe(
        response => {
          if (response != null) {
            this.recordDatePeriod = response;
            this.isSuccessful = true;
            this.isSignUpFailed = false;
            this.msg="Request Sended Successfully";
            this.reloadPage();
          }
        
        },
        err => {
          this.errorMessage = "no Record Found.";
          this.isSignUpFailed = true;
        }
      );
    }
    exportWorkLog(work): void {
      this.username = this.tokeService.getUser().username;
      if(work){
        this.taskService.downloadBillingLogMonth(this.form).subscribe(
          response => {
            this.workLogData=response;
            this.taskService.exportAsExcelFile(this.workLogData, this.username);
            this.msg="File Downloaded";
          }
         );

      }else{
        this.taskService.downloadBilingDate(this.form).subscribe(
          response => {
            this.workLogData=response;
            this.taskService.exportAsExcelFile(this.workLogData, this.username);
            this.msg="File Downloaded";
          }
         );
      }
      
    }
    reloadPage() {
      window.location.reload();
    }

}
