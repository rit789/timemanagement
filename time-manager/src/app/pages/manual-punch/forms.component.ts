import { Component, OnInit} from '@angular/core';
import { TaskService } from 'src/app/services/task.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-forms',
  templateUrl: './forms.component.html',
  styleUrls: ['./forms.component.scss']
})
export class FormsComponent implements OnInit {
  task: any[];
  form: any = {};
  isSuccessful = false;
  isSignUpFailed = false;
  errorMessage = '';
  editField: string;
  bsModalRef: any;
  postList: any[] = [];
  msg:string

  constructor(private taskService: TaskService,private router:Router) { }
  ngOnInit() {
  }
  manualPunch() {
    this.taskService.manualPunch(this.form).subscribe(
      data => {
       
        this.isSuccessful = true;
        this.msg=data.message
        this.isSignUpFailed = false;
        this.router.navigate(['/dashboard']);
        //this.reloadPage() 
        
      },
      err => {
        this.errorMessage = err.error.message;
        this.isSignUpFailed = true;
        this.msg=err.error.message;
      }
    );
  }
  reloadPage() {
    window.location.reload();
  }
}
