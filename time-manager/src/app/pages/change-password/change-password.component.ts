import { Component, OnInit } from '@angular/core';
import { AuthService } from 'src/app/services/auth.service';
import { TokenStorageService } from 'src/app/services/token-storage.service';
import { Route } from '@angular/compiler/src/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-change-password',
  templateUrl: './change-password.component.html',
  styleUrls: ['./change-password.component.scss']
})
export class ChangePasswordComponent implements OnInit {
  form: any = {};
  isSuccessful = false;
  isSuccess = false;
  isSuccessFailed=false;
  isSignUpFailed = false;
  errorMessage = '';
  msg:string;
  isLoggedIn = false;
  isLoginFailed = false;
  constructor(private authService:AuthService,private tokeService:TokenStorageService ,private router:Router) { }

  ngOnInit() {
    if (this.tokeService.getToken()) {
      this.isLoggedIn = true;
    }
  }
  changePassword() {
    this.authService.changePassword(this.form).subscribe(
      data => {
        this.isSuccessful = true;
        this.isSignUpFailed = false;
        this.msg=data.message;
        this.router.navigate(['/dashboard']);
        //this.reloadPage();
      },
      err => {
        this.errorMessage = err.error.message;
        this.isSignUpFailed = true;
      }
    );
  }
 getOtp(email) {
    this.authService.getOtp(email).subscribe(
      data => {
        this.isSuccess = true;
        this.msg=data.message;
        this.isSignUpFailed = false;
      },
      err => {
        this.errorMessage = err.error.message;
        this.isSuccessFailed = true;
        this.msg=err.message;
      }
    );
  }
  reloadPage() {
    window.location.reload();
  }
}
