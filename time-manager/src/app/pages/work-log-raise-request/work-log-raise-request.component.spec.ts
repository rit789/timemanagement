import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { WorkLogRaiseRequestComponent } from './work-log-raise-request.component';

describe('WorkLogRaiseRequestComponent', () => {
  let component: WorkLogRaiseRequestComponent;
  let fixture: ComponentFixture<WorkLogRaiseRequestComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ WorkLogRaiseRequestComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(WorkLogRaiseRequestComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
