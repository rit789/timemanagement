import { Component, OnInit } from '@angular/core';
import { TaskService } from 'src/app/services/task.service';
import { Router } from '@angular/router';
import { TokenStorageService } from 'src/app/services/token-storage.service';

@Component({
  selector: 'app-work-log-raise-request',
  templateUrl: './work-log-raise-request.component.html',
  styleUrls: ['./work-log-raise-request.component.scss']
})
export class WorkLogRaiseRequestComponent implements OnInit {
  recordDatePeriod: any[];
  worklogsMonth: any[];
  form: any = {};
  isSuccessful:boolean = false;
  isSignUpFailed = false;
  errorMessage = '';
  editField: string;
  bsModalRef: any;
  msg: string;
  isLoggedIn = false;
  isLoginFailed = false;
  username: string;
  year: any = ['2020', '2021', '2022', '2023', '2024', '2025', '2026', '2027', '2028', '2029', '2030'];
  month: any =
    ['JANUARY', 'FEBRUARY', 'MARCH', 'APRIL', 'MAY', 'JUNE', 'JULY', 'AUGUST', 'SEPTEMBER', 'OCTOBER', 'NOVEMBER', 'DECEMBER'];
  constructor(private taskService: TaskService, private tokeService: TokenStorageService, private router: Router) {
      if (this.tokeService.getToken()) {
        this.isLoggedIn = true;
        this.username = this.tokeService.getUser().username;
      }
  }
  ngOnInit() {

  }

  getWorkLogMonth() {
      this.taskService.getWorkLogMonth(this.form).subscribe(
        response => {
          if (response!==null) {
            console.log("not null");
            this.worklogsMonth = response;
            this.isSuccessful = true;
            this.isSignUpFailed = false;
            this.msg="Request Raised Successfully";
            this.reloadPage();
          }
        },
        err => {
          this.errorMessage = "No Record Found.";
          this.isSignUpFailed = true;
        }
      );
    }
    getWorkLogDate() {
      this.taskService.getWorkLogDate(this.form).subscribe(
        response => {
          if (response != null) {
            this.recordDatePeriod = response;
            this.isSuccessful = true;
            this.isSignUpFailed = false;
            this.msg="Request Raised Successfully";
            this.reloadPage();
          }
        
        },
        err => {
          this.errorMessage = "no Record Found.";
          this.isSignUpFailed = true;
        }
      );
    }
    reloadPage() {
      window.location.reload();
    }
}
