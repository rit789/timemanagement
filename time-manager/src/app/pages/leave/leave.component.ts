import { Component, OnInit } from '@angular/core';
import { TaskService } from 'src/app/services/task.service';
import { AuthService } from 'src/app/services/auth.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-leave',
  templateUrl: './leave.component.html',
  styleUrls: ['./leave.component.scss']
})
export class LeaveComponent implements OnInit {
  isLoggedIn = false;
  showAdminBoard = false;
  showModeratorBoard = false;
  username: string;
  email: string;
  role: string;
  extra: string;
  logInDate: string
  less: string;
  task: any[];
  form: any = {};
  isExpanded = true;
  showSubmenu: boolean = false;
  isShowing = false;
  showSubSubMenu: boolean = false;
  isSuccessful = false;
  isSignUpFailed = false;
  errorMessage = '';
  to:string
  msg:string
  constructor(private taskService: TaskService,private authService:AuthService,private router:Router) { }

  ngOnInit(): void {
    this.getUser()
    
  }
  applyLeave() {
    console.log(this.to);
    this.taskService.applyLeave(this.form).subscribe(
      data => {
        this.isSuccessful = true;
        this.msg=data.message;
        this.isSignUpFailed = false;
        this.router.navigate(['/dashboard']);
      },
      err => {
        this.errorMessage = err.error.message;
        this.isSignUpFailed = true;
        this.msg=err.error.message;
      }
    );
  }
  reloadPage() {
    window.location.reload();
  }
  getUser() {
    this.authService.getProfile().subscribe(
      data => {
        this.to=data.managerEmail;
      }
    );
  }
}
