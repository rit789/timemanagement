import { Component, OnInit, HostListener } from '@angular/core';
import { TaskService } from 'src/app/services/task.service';
import { TokenStorageService } from 'src/app/services/token-storage.service';
import { AuthService } from 'src/app/services/auth.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.scss']
})
export class DashboardComponent implements OnInit {
  task: any[];
  form: any = {};
  isSuccessful = false;
  isSignUpFailed = false;
  errorMessage = '';
  editField: string;
  bsModalRef: any;
  data: any;
  msg: string;
  postList: any[] = [];
  active: boolean;
  isLoggedIn = false;
  isLoginFailed = false;
  isDownload =false;
  username: string;
  roles: string[] = [];
  returnUrl: string;
  excelData: string[] = [];
  workLogData: string[] = [];
  worklogs: any[];
  sum:any;
  
  year: any = ['2020', '2021', '2022', '2023', '2024', '2025', '2026', '2027', '2028', '2029', '2030'];
  month: any =
    ['JANUARY', 'FEBRUARY', 'MARCH', 'APRIL', 'MAY', 'JUNE', 'JULY', 'AUGUST', 'SEPTEMBER', 'OCTOBER', 'NOVEMBER', 'DECEMBER'];
  constructor(private taskService: TaskService, private tokeService: TokenStorageService, private router: Router) {
      if (this.tokeService.getToken()) {
        this.isLoggedIn = true;
        this.roles = this.tokeService.getUser().roles;
        this.username = this.tokeService.getUser().username;
        this.getTask();
        this.getWorkLog();
      }
  }
  ngOnInit() {

  }
  onCreated() {
    this.taskService.register(this.form).subscribe(
      data => {
        this.msg = data.message
        this.isSuccessful = true;
        this.isSignUpFailed = true;
        this.router.navigate(['/dashboard']);
      },
      err => {
        this.errorMessage = err.error.message;
        this.isSignUpFailed = false;
      }
    );
  }
  manualPunch() {
    this.taskService.manualPunch(this.form).subscribe(
      data => {
        console.log(data);
        this.isSuccessful = true;
        this.msg = data.message;
        this.isSignUpFailed = data.message;
        this.router.navigate(['/dashboard']);
      },
      err => {
        this.errorMessage = err.error.message;
        this.isSignUpFailed = true;
      }
    );
  }
  getTask() {
    this.taskService.getTask(this.form).subscribe(
      data => {
        if (data != null) {
          this.sum=data.spendHrs;
          this.task = data;
          this.excelData = data;
          this.isSignUpFailed = false;
        }
        else {
          this.isSignUpFailed = true;
          this.msg = "No Record Found";
        }
      },
      err => {
        this.errorMessage = err.error.message;
        this.isSignUpFailed = true;
      }
    );
  }

  updateList(employee: any) {
    console.log(employee);
    this.taskService.updatePost(employee).subscribe(
      data => {
        this.isSuccessful = true;
        this.msg = data.message;
        this.isSignUpFailed = false;
        this.reloadPage();

      },
      err => {
        this.errorMessage = err.error.message;
        this.isSignUpFailed = true;
      }
    );
  }

 
  autoPunch() {

    this.taskService.register(this.form).subscribe(
      data => {
        console.log(data);
        this.isLoggedIn = true;
      },
      err => {
        this.errorMessage = err.error.message;
      }
    );
  }
  exportAsXLSX(): void {
   
    this.getTask();
    this.taskService.exportAsExcelFile(this.excelData, this.username);

  }
  exportWorkLog(): void {
 
    this.getWorkLog();
    this.taskService.exportAsExcelFile(this.workLogData, this.username);

  }
  getWorkLog() {
      this.taskService.getWorkLog(this.form).subscribe(
        data => {
          if (data != null) {
            this.worklogs = data;
            this.workLogData = data;
            this.isSignUpFailed = false;
          }
          else {
            this.isSignUpFailed = true;
            this.msg = "No Record Found";
          }
        },
        err => {
          this.errorMessage = err.error;
          this.isSignUpFailed = true;
        }
      );
    }
  
    updateWorkLog(work: any) {
      console.log(work);
      this.taskService.workLog(work).subscribe(
        response => {
          this.isSuccessful = true;
          this.msg = response.message;
          this.isSignUpFailed = false;
          this.reloadPage();
         },
         err => {
           this.errorMessage = err.error.message;
           this.isSignUpFailed = true;
         }
       );
     }
     reloadPage() {
      window.location.reload();
    }
}
