import { Component, OnInit } from '@angular/core';
import { AuthService } from '../../services/auth.service';
import { TokenStorageService } from '../../services/token-storage.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-profile',
  templateUrl: './profile.component.html',
  styleUrls: ['./profile.component.scss']
})
export class ProfileComponent implements OnInit {
  msg:string;
  form: any = {};
  data:any;
  username:string
  isSuccessful = false;
  isSignUpFailed = false;
  isLoggedIn = false;
  constructor(private authService:AuthService,private tokenStorageService:TokenStorageService,private router:Router) { }
  ngOnInit() {
    this.username = JSON.parse(sessionStorage.getItem('auth-user')).username;
    this.isLoggedIn = !!this.tokenStorageService.getToken();
    if (this.isLoggedIn) {
      const user = this.tokenStorageService.getUser();
      this.username = user.username;
      this.getUser() 
    }
  }
updateUser(user :any) {
  console.log(user);
    this.authService.updateUser(user).subscribe(
      data => {
        this.msg=data.message
        this.isSuccessful = true;
        this.isSignUpFailed = true;
        this.router.navigate(['/dashboard']);
        this.reloadPage();
      },
      err => {
        this.msg = err.message;
        this.isSignUpFailed = false;
      }
    );
  }
  getUser() {
    this.authService.getProfile().subscribe(
      data => {
        this.data=data;
      }
    );
  }
  reloadPage() {
    window.location.reload();
  }
}
